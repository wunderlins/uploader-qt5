#include "uploader.h"
#include "ui_uploader.h"
#include <QFileDialog>
#include <QTimer>
#include <QRegularExpression>
#include <QJsonObject>
#include <QJsonArray>
#include <QStyle>
#include <QDesktopServices>

#if defined(Q_OS_WIN)
#include <windows.h>
#endif

QString Uploader::error = QString();
UploaderLib::UploaderLibError Uploader::errorCode = UploaderLib::OK;
Uploader* Uploader::instance = nullptr;
bool Uploader::execRun = false;

Uploader::Uploader(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Uploader)
    , m_thread(this)
{
    ui->setupUi(this);

    connect(&m_thread, &UploadThread::progressChanged, this, &Uploader::updateProgress);
    connect(&m_thread, &UploadThread::log, this, &Uploader::log);
    connect(&m_thread, &UploadThread::httpResponse, this, &Uploader::httpResponse);
    connect(&m_thread, &UploadThread::uploadError, this, &Uploader::uploadError);
    //connect(&UploaderLib, &UploaderLib::shutdown, this, &Uploader::shutdown);

    // set icon for error messages:
    QIcon stdErrorIcon = this->style()->standardIcon(QStyle::SP_MessageBoxCritical, nullptr, nullptr);
    QPixmap pixmap = stdErrorIcon.pixmap(stdErrorIcon.actualSize(QSize(32, 32)));
    ui->error_lbl->setPixmap(pixmap);

    // branding
#if defined(Q_OS_WIN)
    // for windows, add progress bar in the taskbar entry
    m_button = new QWinTaskbarButton(this);
    m_button->setWindow(this->windowHandle());
    //m_button->setOverlayIcon(QIcon(":/resources/images/icon.png"));
    mw_progress = m_button->progress();
    mw_progress->setVisible(true);
#else
    this->setWindowIcon(QIcon(":/resources/images/icon.png"));
#endif

    // this is our startup handler, we start uploading once thew windows is
    // initialized and all events in the queue are prcessed after show
    QTimer::singleShot(0, this, SLOT(startup()));
}

Uploader::~Uploader()
{
    delete ui;
}

void Uploader::timerEvent(QTimerEvent *event) {
    //qDebug() << "Timer ID:" << event->timerId();

    float current_progress = m_progressThresholdLast;
    float remain = 100 - current_progress;
    float new_progress = current_progress + (remain * 0.1);
    m_progressThresholdLast = new_progress;
    ui->progressBar->setValue((int) new_progress);
}

Uploader *Uploader::getInstance() {
    if (!instance) {
        instance = new Uploader();
    }

    return instance;
}

int Uploader::handle_error(UploaderLib::UploaderLibError errorCode, QString message) {
    QCoreApplication *a = QApplication::instance();
    Uploader *w = Uploader::getInstance();

    Uploader::errorCode = errorCode;
    Uploader::error = message;

    w->ui->progressBar->setValue(0);
    w->update_gui();
    fprintf(stderr, "%s\n", message.toStdString().c_str());
    w->show();
    w->log(message);

    int exitCode = 0;
    if (!Uploader::execRun) {
        Uploader::execRun = true;
        exitCode = a->exec();
    }

    if (Uploader::errorCode != UploaderLib::OK)
        return Uploader::errorCode;

    return exitCode;
}

#if defined(Q_OS_WIN)
void Uploader::showEvent(QShowEvent *e)
{
#ifdef Q_OS_WIN32
    m_button->setWindow(windowHandle());
#endif
    e->accept();
}
#endif

void Uploader::uploadError(int errorCode, QString message) {
    log("Upload error: " + QString::number(errorCode));
    if (errorCode == 0)
        return;

    // make sure the timer is stopped
    if (m_timerid != -1) {
        killTimer(m_timerid);
        ui->progressBar->setValue(0);
    }

    // handle upload errors
    if (errorCode == 1) {
        Uploader::handle_error(UploaderLib::NETWORK_FILE_READ, "Could not read upload file.");
        return;
    } else if (errorCode == 2) {
        Uploader::handle_error(UploaderLib::NETWORK_STATUS_NOT_200, "HTTP Response Status not 200.");
        return;
    } else if (errorCode == 3) {
        Uploader::handle_error(UploaderLib::NETWORK_FILE_READ, "Upload interruped or otherwise failed.");
        return;
    } else if (errorCode == 4) {
        Uploader::handle_error(UploaderLib::NETWORK_FAILED_TO_CONNECT, "Failed to connect to Server.");
        return;
    } else if (errorCode == 5) {
        Uploader::handle_error(UploaderLib::NETWORK_FAILED_TO_CONNECT, "Failed to connect to Server, reason: " + message);
        return;
    } else {
        Uploader::handle_error(UploaderLib::NETWORK_UNKNOWN, "General Network Error: " + QString::number(errorCode));
        return;
    }
}

void Uploader::startup() {
    // set title and header
    this->setWindowTitle(config.application_title + " " + STRINGIFY(VERSION));
    ui->progname->setText(config.application_header);
    if (config.application_copyright != "")
        ui->copyright->setText(config.application_copyright);
    else
        ui->copyright->hide();
    if (config.application_text != "")
        ui->text->setText(config.application_text);
    else
        ui->copyright->hide();

    // if we have a custom branding image, try to set it
    if (config.application_image != "") {
        QFile image(programArguments.exe_dir.fileName() + "/" + config.application_image);
        //qDebug() << "image: " << image.fileName();
        // check if file exists and is readable
        if (image.exists()) {
            //qDebug() << "creating pixmap";
            QPixmap pimage(image.fileName());
            ui->image->setPixmap(pimage);
        } else {
            log("application-image is not readable: " + image.fileName());
        }
    }

    this->update_gui();
    this->show();

    log("Version info", true);
    log(QString("Program name: ").append(STRINGIFY(NAME)));
    log(QString("Program version: ").append(STRINGIFY(VERSION)));
    log(QString("Git version: ").append(STRINGIFY(GIT_VERSION)));

    // log command line arguments
    log("Command line parameters", true);
    log("mode: " + programArguments.mode);
    log("exe_dir: " + programArguments.exe_dir.fileName());
    log("config_file: " + programArguments.config_file);
    log("file: " + programArguments.file.fileName());
    log("post_data_file: " + programArguments.post_data_file);
    log("show_console: " + QString::number(programArguments.show_console));
    log("dragndrop_file: " + QString::number(programArguments.dragndrop_file));
    log("Command line Variables (--name=value): ");
    for(QHash<QString, QString>::const_iterator i = programArguments.parameter.constBegin();
        i != programArguments.parameter.constEnd(); i++) {
        log(" - " + i.key() + " = " + i.value());
    }

    // log config options
    QString exit_codes("");
    for (auto const& c: config.pre_command_exit_codes)
        exit_codes.append(QString::number(c) + " ");
    log("Parsed configuration settings", true);
    log(" - timeout: "  + QString::number(config.timeout));
    log(" - default_mode: "  + config.default_mode);
    log(" - http_to_stdout: " + QString::number(config.http_to_stdout));
    log(" - url: " + config.url);
    log(" - file_form_name: " + config.file_form_name);
    log(" - file_min_size: " + QString::number(config.file_min_size));
    log(" - file_max_size: " + QString::number(config.file_max_size));
    log(" - post_data_name: " + config.post_data_name);
    log(" - post_data_content_type: " + config.post_data_content_type);
    log(" - user_select: " + QString::number(config.user_select));
    log(" - command: "  + config.command);
    log(" - pre_command: "  + config.pre_command);
    log(" - pre_command_exit_codes: " + exit_codes);
    log(" - application_title: "  + config.application_title);
    log(" - application_header: "  + config.application_header);
    log(" - application_copyright: "  + config.application_copyright);
    log(" - application_text: "  + config.application_text);
    log(" - application_text_error: "  + config.application_text_error);
    log(" - application_image: "  + config.application_image);

    log("Request Header (header-*): ");
    for(QHash<QString, QString>::const_iterator i = config.header.constBegin();
        i != config.header.constEnd(); i++) {
        log(" - " + i.key() + " = " + i.value());
    }
    log("Result Variables (resultvar-*): ");
    for(QHash<QString, QString>::const_iterator i = config.result_var.constBegin();
        i != config.result_var.constEnd(); i++) {
        log(" - " + i.key() + " = " + i.value());
    }
    log("Post Data\n - post_data: " + config.post_data);

    // start Uploading
    if (!config.user_select && error.isEmpty())
        on_upload_clicked();
}

/*
void Uploader::shutdown(int exit_code) {
    QApplication::exit(exit_code);
}
*/

void Uploader::httpResponse(QString responseHeaderAndBody) {
    killTimer(m_timerid);
    ui->progressBar->setValue(100);

    log("Raw HTTP Response", true);
    log(responseHeaderAndBody);

    log("Parsing response", true);

    // parse response
    httpResponse_t res = UploaderLib::parseHtttpResponse(responseHeaderAndBody);

    if (res.error != 0) {
        Uploader::handle_error(UploaderLib::NETWORK_UNKNOWN, "Http Communication error: " + res.errorMessage);
        return;
    }

    if (res.statusCode != 200) {
        Uploader::handle_error(UploaderLib::NETWORK_STATUS_NOT_200, "http status code: " + QString::number(res.statusCode));
        return;
    }

    // command is optional, if it is not configured or is empty then we
    // are done here
    if (config.command == "") {
        log("finished, command is empty, not executing");
        UploaderLib::pruneLogFile();
        QApplication::exit(0);
        return;
    }

    if (res.contentType != "application/json") {
        Uploader::handle_error(UploaderLib::NETWORK_WRONG_CONTENTTYPE, "Response contains no JSON: " + res.contentType);
        return;
    }

    // find variables in result, resolve paths in resultvar
    jsonResultVar_t varres = UploaderLib::findResultVars(config.result_var, res.body);
    if (varres.error != 0) {
        Uploader::handle_error(UploaderLib::RESPONSE_PARSE_JSON, "Failed to parse response json, reason: " + varres.errorMessage);
        return;
    }

    // get the command, substitute all parameter variables and we are done
    varSubtitution_t cmdres = UploaderLib::substituteVariables(varres.variables, config.command);
    if (cmdres.error != 0) {
        Uploader::handle_error(UploaderLib::RESPONSE_VARIABLE_SUBSTITUTE, "failed to replace variables, reason: " + cmdres.errorMessage);
        return;
    }

    // now, substitute environment variables
    QString cmd = UploaderLib::expandEnvironmentVariables(cmdres.value);

    // all fine and dandy, launch the browser with the result url and exit
    log("Command variables substituted:\n[" + cmd + "]");

    // decide how to run the command, if it starts with
    // "[a-zA-Z0-9]+://" we trat it as url and let the shell decided
    // what to do with it.
    //
    // else, we treat it as an executable command line.
    QRegularExpression re("^[a-zA-Z0-9]+://");
    QRegularExpressionMatch match = re.match(cmd);
    if (match.hasMatch()) {
        log("Opening url.");
        QDesktopServices::openUrl(QUrl(cmd));
    } else {
        log("Running command");

        QStringList arguments = UploaderLib::splitCommandLine(cmd);

        if (arguments.size() == 0) {
            Uploader::handle_error(UploaderLib::EXECUTE_QUTE_ERROR, "Failed to processs command.");
            return;
        }

        QString c = arguments.at(0);
        arguments.pop_front();

        // run the command
        bool ret = QProcess::startDetached(c, arguments);

        // check for success
        if (!ret) {
            Uploader::handle_error(UploaderLib::EXECUTE_COMMAND_FAILED_TO_START, "failed to execute command.");
            return;
        }
    }

    log("Finished successfully, exiting with exit code 0");
    UploaderLib::pruneLogFile();
    QApplication::exit(0);
}

void Uploader::window_to_front() {
    this->show();

#if defined(Q_OS_WIN)

    QString windowTitle = this->windowTitle();
    HWND hwnd = ::FindWindowA(NULL, windowTitle.toLocal8Bit());

    // https://forum.qt.io/topic/1939/activatewindow-does-not-send-window-to-front/5
    // THIS IS A HACK:
    // from QT documentation:
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // void QWidget::activateWindow ()
    // ...
    // On Windows, if you are calling this when the application is not currently
    // the active one then it will not make it the active window. It will change
    // the color of the taskbar entry to indicate that the window has changed in
    // some way. This is because Microsoft do not allow an application to
    // interrupt what the user is currently doing in another application.
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // This hack does not give the focus to the app but brings it to front so
    // the user sees it.
    ::SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
    ::SetWindowPos(hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
    // HACK END

    raise();
    this->show();

    qDebug() << "hwnd: " << hwnd;
    if (hwnd != NULL) {
        //if (::IsWindowVisible(hwnd)) {
            ::SwitchToThisWindow(hwnd, TRUE);
        //}
    }


#endif

    raise();
    // Qt::WindowStaysOnTopHint
    this->setWindowState( (this->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
    this->raise();  // for MacOS
    this->activateWindow(); // for Windows
    this->show();
    raise();
}

void Uploader::update_gui() {
    ui->log->setHidden(programArguments.show_console ? false : true);

    // hide file selection on error or if not configured
    if (!config.user_select || error.length() > 0) {
        ui->file->hide();
        ui->upload->hide();
        ui->select->hide();
    }

    // we have a startup error, show it
    if (error.length() > 0) {
        ui->error_msg->setText(QString::number(errorCode) + ": " + error);
        ui->progressBar->setDisabled(true);
        ui->error_lbl->show();
        ui->error_msg->show();
        //ui->text->hide();
        ui->text->setText(config.application_text_error);

        // show the error code
        //ui->progname->setText("Error: " + QString::number(errorCode));

        window_to_front();
        return;
    } else {
        ui->error_lbl->hide();
        ui->error_msg->hide();

        window_to_front();
    }

    window_to_front();
}

void Uploader::on_upload_clicked()
{
    log("Starting upload", true);

    QString payload = UploaderLib::preparePayload(config.post_data, programArguments.parameter);
    log("Payload: " + payload);

    ui->statusbar->showMessage("Uploading ...");
    m_thread.upload(programArguments.file.fileName(), config.timeout, payload, config.header);
}

void Uploader::on_select_clicked()
{
    auto fileName = QFileDialog::getOpenFileName(this,
        "Add File", "", "HealthEngine (*.pdf *.zip);;PDF (*.pdf);;ZIP Archive (*.zip)");
    log("File: " + fileName + " selected");
    ui->file->setText(fileName);
    programArguments.file.setFileName(fileName);
}

void Uploader::log(QString msg, bool section) {
    if (section) {
        ui->log->appendPlainText("_______________________________________________");
        qInfo() << "_______________________________________________";
        msg = "==> " + msg.replace("\\r", "\r").replace("\\n", "\n");
    } else {
        msg = msg.replace("\\r", "\r").replace("\\n", "\n");
    }
    qInfo() << "log(): " << msg;
    ui->log->appendPlainText(msg);

    // write to log file:
    if (programArguments.log_file_open) {
        QTextStream out(&programArguments.log_file);
        out << QDateTime::currentDateTime().toString(Qt::ISODate) << " " << msg << "\n";
    }
}

void Uploader::on_progressBar_valueChanged(int value)
{
    //log("Progress " + QString::number(value) + "%");
#if defined(Q_OS_WIN)
    mw_progress->setValue(value);
#endif
}

void Uploader::updateProgress(int value)
{
    /*
    if (value < 100) {
        m_progressThresholdLast = value * m_progressThreshold / 100;
        ui->progressBar->setValue(m_progressThresholdLast);
    } else if (m_timerid == -1 && Uploader::errorCode == UploaderLib::OK) {
        qDebug() << "Starting Timer";
        ui->statusbar->showMessage("Processing ...");
        m_timerid = startTimer(500); // 500 milliseconds
    } */

    if (value < 100) {
        ui->progressBar->setValue(value);
    } else if (m_waiting == false && Uploader::errorCode == UploaderLib::OK) {
        // test busy indicator
        m_waiting = true;
        ui->progressBar->setMinimum(0);
        ui->progressBar->setMaximum(0);
        ui->progressBar->reset();
    }
}

bool Uploader::eventFilter(QObject *Object, QEvent *Event) {
  if (Event->type() == QEvent::KeyPress) {
    QKeyEvent *KeyEvent = (QKeyEvent*)Event;

    switch(KeyEvent->key()) {
      case Qt::Key_F12:
        //log("Uploader::eventFilter()");
        //ui->log->setHidden(ui->log->isVisible() ? true : false);

        if (ui->log->isVisible()) {
            ui->log->setHidden(true);
            ui->text->setHidden(false);
            ui->copyright->setHidden(false);
        }  else {
            ui->log->setHidden(false);
            ui->text->setHidden(true);
            ui->copyright->setHidden(true);
        }
        return true;
    }
  }

  return false;
}
