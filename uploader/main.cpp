#include <stdio.h>
#include "uploader.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QFile>
#include <QSettings>
#include <QStringList>
#include <QDebug>
#include <QSysInfo>
#include <QStandardPaths>
#include <QDir>

#if defined(Q_OS_MACOS)
#include "osxapplication.h"
#endif

#if defined(Q_OS_WIN)
#include <stdio.h>
#include <string.h>
#include <cstdio>
#include <windows.h>
#include <tlhelp32.h>

bool process_already_running(const char *executable_name) {
    int found = 0;
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

#ifdef UNICODE
    const size_t cSize = strlen(executable_name);
    wchar_t* wc1 = new wchar_t[cSize+1];
    mbstowcs (wc1, executable_name, cSize);
    //printf("wc: %ls\n", wc);
#endif

    if (Process32First(snapshot, &entry) == TRUE) {
        while (Process32Next(snapshot, &entry) == TRUE) {
            //if (wcscmp(entry.szExeFile, wc1) == 0) {
#ifdef UNICODE
            if (wcscmp(entry.szExeFile, wc1) == 0) {
#else
            if (strcmp(executable_name, entry.szExeFile) == 0) {
#endif
                //printf("executable_name: %s, entry.szExeFile: %ls\n", executable_name, entry.szExeFile);
                found++;
            }
        }
    }

    CloseHandle(snapshot);
    return (found > 1);
}
#endif

int main(int argc, char *argv[])
{
    // initializes Qt5 Application
#if defined(Q_OS_MACOS)
    OsxApplication a(argc, argv);
#else
    QApplication a(argc, argv);
#endif

    QApplication::setApplicationName(STRINGIFY(NAME));
    QApplication::setApplicationVersion(STRINGIFY(VERSION));

    // get log directory
    QString logfilePath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    qDebug() << "logfilePath: " << logfilePath;
    bool folderExists = QFileInfo::exists(logfilePath);
    if (!folderExists) {
        QDir logDir(logfilePath);
        folderExists = logDir.mkpath(logfilePath);
    }

    // if we have a log directory, open log file
    if (folderExists) {
        programArguments.log_file.setFileName(logfilePath + "/uploader.log");
        programArguments.log_file_open = programArguments.log_file.open(QIODevice::WriteOnly | QIODevice::Append);

        if (programArguments.log_file_open) {
            // add a newline
            programArguments.log_file.write("\n==== logger started ====\n");
        }
    }

    // find executable directory and config file
    QString exedir = QApplication::applicationDirPath();
    qputenv("UPLOADER_HOME", exedir.toStdString().c_str());
    UploaderLib::UploaderLibError ret = UploaderLib::applicationInit(exedir);

    // create window instance
    //Uploader w;
    Uploader *w = Uploader::getInstance();

    // register global key event handler for F12 (open console)
    QApplication::instance()->installEventFilter(w);

#if defined(Q_OS_WIN)
    // Remove directory if present.
    // Do this before extension removal incase directory has a period character.
    std::string filename = std::string(argv[0]);
    const size_t last_slash_idx = filename.find_last_of("\\/");
    if (std::string::npos != last_slash_idx) {
        filename.erase(0, last_slash_idx + 1);
    }
    const char *f = filename.c_str();
    //printf("f: %s\n", f);
    bool running = process_already_running(f);

    if (running)
        return Uploader::handle_error(UploaderLib::ALREADY_RUNNING, "Process is already running.");
#endif

    // check if config file exists
    if (ret != UploaderLib::OK)
        return Uploader::handle_error(UploaderLib::CONFIG_NOT_READABLE, "Config file not readable");

    // parse command line arguments ////////////////////////////////////////////////////

    // do we have an url to parse?
    // this is the case if uploader.exe was called as url handler
    //bool is_url = false;
    QRegExp re("^[a-zA-Z_]+://.*$");  // proto://.*
    if (argc > 0 && re.exactMatch(argv[1])) {
        ret = UploaderLib::parseUrl(argv[1]);
        qDebug() << "Parsed url: " << ret;

        // check for errors
        if (ret == UploaderLib::URLPARSE_MALFORMED_URL)
            return Uploader::handle_error(UploaderLib::URLPARSE_MALFORMED_URL, "Failed to parse url: " + QString(argv[1]));

        if (ret == UploaderLib::URLPARSE_NO_FILE)
            return Uploader::handle_error(UploaderLib::URLPARSE_NO_FILE, "Missing file attribute in url: " + QString(argv[1]));

        if (ret == UploaderLib::URLPARSE_NO_MODE)
            return Uploader::handle_error(UploaderLib::URLPARSE_NO_MODE, "Missing mdoe in url: " + QString(argv[1]));
        //is_url = true;

    } else { // program was called directly with command line args
        ret = UploaderLib::parseCommandLineArguments(QApplication::arguments());
        qDebug() << "Parsed cmd: " << ret;

        if (ret == UploaderLib::NO_MODE)
            return Uploader::handle_error(UploaderLib::NO_MODE, "Invalid mode, `-m=`.");

        if (ret == UploaderLib::SHOW_HELP)
            return 0;
    }

    // read settings file //////////////////////////////////////////////////////////////
    ret = UploaderLib::readSettings();
    if (ret == UploaderLib::SETTINGS_NOT_EXIST)
        return Uploader::handle_error(UploaderLib::SETTINGS_NOT_EXIST, "Config file does not exist.");

    if (ret == UploaderLib::SETTINGS_NO_MODE)
        return Uploader::handle_error(UploaderLib::SETTINGS_NO_MODE, "No mode provided, check `-m=...`.");

    if (ret == UploaderLib::SETTINGS_PAYLOAD_NOT_EXIST)
        return Uploader::handle_error(UploaderLib::SETTINGS_PAYLOAD_NOT_EXIST, "Payload file `post-data` not found.");

    if (ret == UploaderLib::SETTINGS_PAYLOAD_NOT_READABLE)
        return Uploader::handle_error(UploaderLib::SETTINGS_PAYLOAD_NOT_READABLE, "Payload file `post-data` is not readable.");

    if (ret == UploaderLib::SETTINGS_PARSE_ERROR)
        return Uploader::handle_error(UploaderLib::SETTINGS_PARSE_ERROR, "Failed to parse config file.");

    if (ret == UploaderLib::SETTINGS_INVALID_MODE)
        return Uploader::handle_error(UploaderLib::SETTINGS_INVALID_MODE, QString("Mode '%1' is not configured.").arg(programArguments.mode));

    if (ret == UploaderLib::SETTINGS_INVALID_CONFIGURATION)
        return Uploader::handle_error(UploaderLib::SETTINGS_INVALID_CONFIGURATION, "Configuration error.");

    // pre-command: launch a command (blocking) and wait for success:
    if (config.pre_command != "") {
        UploaderLib::UploaderLibError res = UploaderLib::preExecute(config.pre_command);

        if (res != UploaderLib::OK) {
            if (res == UploaderLib::EXECUTE_CMD_EMPTY)
                return Uploader::handle_error(UploaderLib::EXECUTE_CMD_EMPTY, QString("Empty pre-command: %1").arg(res));

            if (res == UploaderLib::EXECUTE_QUTE_ERROR)
                return Uploader::handle_error(UploaderLib::EXECUTE_QUTE_ERROR, QString("Unbalanced Quotes in pre-command: %1").arg(res));

            if (res == UploaderLib::EXECUTE_PROCESS_FAILED_TO_START)
                return Uploader::handle_error(UploaderLib::EXECUTE_PROCESS_FAILED_TO_START, QString("Failed to launch pre-command: %1").arg(res));

            if (res == UploaderLib::EXECUTE_PROCESS_FINISHED_UNECPECTED)
                return Uploader::handle_error(UploaderLib::EXECUTE_PROCESS_FINISHED_UNECPECTED, QString("pre-command process finished unexpected: %1").arg(res));

            return Uploader::handle_error(UploaderLib::FAILED_PRECOMMAND, QString("pre-command process finished with invalid exit code: %1").arg(res));
        }
    }

    // check if file exists and is readable, else exit(1)
    // when user can select a file, we do not abort
    UploaderLib::UploaderLibError res = UploaderLib::fileIsReadable();
    if (res == UploaderLib::FILE_PARAM_MISSING)
        return Uploader::handle_error(UploaderLib::FILE_PARAM_MISSING, "No file specified");

    if (res == UploaderLib::FILE_NOT_EXISTS)
        return Uploader::handle_error(UploaderLib::FILE_NOT_EXISTS, "File '"+programArguments.command_line_args.at(0)+"' does not exist");

    if (res == UploaderLib::FILE_NOT_READABLE)
        return Uploader::handle_error(UploaderLib::FILE_NOT_READABLE, "File '"+programArguments.command_line_args.at(0)+"' is not readable");

    // check file size
    if (config.file_min_size > 0) {
        qint64 s = programArguments.file.size();
        if (s < config.file_min_size)
            return Uploader::handle_error(UploaderLib::FILE_TOO_SMALL, "File '"+programArguments.command_line_args.at(0)+"' is too small: " + QString::number(s) + "bytes.");
    }

    if (config.file_max_size > 0) {
        qint64 s = programArguments.file.size();
        if (s > config.file_max_size)
            return Uploader::handle_error(UploaderLib::FILE_TOO_LARGE, "File '"+programArguments.command_line_args.at(0)+"' is too large: " + QString::number(s) + "bytes.");
    }

    // initialize gui and show it
    // force the window into the foreground
    w->setWindowState( (w->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
    w->raise();  // for MacOS
    w->activateWindow(); // for Windows

    //w->update_gui();
    //w->show();

    Uploader::execRun = true;
    int exitCode = a.exec();

    if (Uploader::errorCode != UploaderLib::OK)
        return Uploader::errorCode;

    return exitCode;
}
