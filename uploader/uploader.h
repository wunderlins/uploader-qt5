#ifndef UPLOADER_H
#define UPLOADER_H

#include <QMainWindow>
#include <QFile>
#include <QHash>
#include <QJsonDocument>
#include <QDateTime>

#include "uploaderlib.h"
#include "uploadthread.h"
#include "globals.h"

#if defined(Q_OS_WIN)
    #include <QWinTaskbarButton>
    #include <QWinTaskbarProgress>
#endif

QT_BEGIN_NAMESPACE
namespace Ui { class Uploader; }
QT_END_NAMESPACE

class Uploader : public QMainWindow
{
    Q_OBJECT

public:
    Uploader(QWidget *parent = nullptr);
    ~Uploader();
    void log(QString msg, bool section = false);
    void update_gui();
    bool eventFilter(QObject *Object, QEvent *Event) override;

    static QString error;
    static UploaderLib::UploaderLibError errorCode;

    static int handle_error(UploaderLib::UploaderLibError errorCode, QString message);
    static Uploader *getInstance();

    static bool execRun;

    void window_to_front();

private slots:
    void on_upload_clicked();
    void on_select_clicked();
    void on_progressBar_valueChanged(int value);
    void updateProgress(int value);
    void httpResponse(QString responseHeaderAndBody);
    //void shutdown(int exit_code);
    void startup();
    void uploadError(int errorCode, QString message = "");

protected:
    void timerEvent(QTimerEvent *event) override;
    int m_timerid = -1;
    int m_progressThreshold = 70;
    float m_progressThresholdLast = 0;

private:
    Ui::Uploader *ui;
    UploadThread m_thread;
    static Uploader* instance;
    bool m_waiting = false;

#if defined(Q_OS_WIN)
    void showEvent(QShowEvent *e);
    QWinTaskbarButton *m_button;
    QWinTaskbarProgress *mw_progress;
#endif

};
#endif // UPLOADER_H
