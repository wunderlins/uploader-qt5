#!/usr/bin/env bash

egrep -n 'FIXME|TODO|WTF' $(find -iname "*.cpp" -o -iname "*.h" | egrep -v "build|/curl|w32deps")
