# Uploader

<img style="float: right" src="resources/uploader.png">

**TODO**: (use windows ccert store)[https://curl.se/mail/meet-2017-03/0030.html]

`Uploader` is a utility that can upload a file as multipart/form-data (RFC7578) to an http server. `Uploader` will show the user the uplaod progress in an OS native window.

Additionaly metadata can be sent along in a separate form field. The additional
metadata payload can be configured in `uploader.ini`. 

`Uploader` can start an aditional program which produces the upload 
content (`pre-command`) before the upload starts. When the process 
finishes, an output file will be used as uplaod content. You will have
to make sure that the generated file is stored in a location where 
`uploader.exe` can find and access it.

## Use cases

This application can be run in 4 modes:

1. **upload**: minimal configuration
    - a document at a predefined location will be uploaded
    - a progress bar is displayed
    - ☞ see [example_uploader_minimal.ini](example_uploader_minimal.ini)
2. **upload & run**: provide all data as command line arguments
    - a document at a predefined location will be uploaded
    - a progress bar is displayed
    - optionally: a follow up program can be run with data from the response.
    - ☞ see [example_uploader_postrun.ini](example_uploader_postrun.ini)
3. **select & upload**: the upload file can be selected by the user
    - program starts with a file selection dialog
    - the document will be uploaded
    - a progress bar is displayed
    - optionally: a follow up program can be run with data from the response.
    - ☞ see [example_uploader_select.ini](example_uploader_select.ini)
4. **run & upload & run**: a second program will be started first, the result will be uploaded
    - a command will be run before upload. you must make sure that the program saves it's document at the location that is provided as `argv[1]`
    - the document will be uploaded
    - a progress bar is displayed
    - optionally: a follow up program can be run with data from the response.

How the program behaves is configured in `uploader.ini` which needs to be located
in the same directory as `uploader.exe`.

![Screenshot Windows](resources/images/screenshot-windows.png)
![Screenshot Linux](resources/images/screenshot-linux.png)

# System Requirements

The client is available for Windows, Linux and OSX (compiles, but mostly untested).

For `uploader` to work, there must be an http server which confirms to the following requirements:

- Must support `HTTP/1.1` (RFC2316)
- Must support `multipart/form-data` (RFC7578)
- Optional: if you want to extract values from the response 
  - Response `Content-Type` must be `application/json`
  - Response Content must be valid JSON

The Release package does not requrie any additional programs or libraries, everything (including c-runtime library) is distributed with the release.

## Running

**IMPORTANT**: on windows, only one `uploader.exe` process is allowed to run. 
If a second process istarted, an error message is displayed and the process exits. 
This is true if the executable file name is `uploader.exe`. If you want to 
prevent this behaviour, rename the executable.

There are 2 ways how `uploader.exe` can be invoked:

- as a command: `uploader[.exe] [[--varnameN=value=N [...]]] [-c] -m=<mode> <path to file>`
- or from an url handler which passes an url into `uploader.exe` as `argv[1]`: `uploader[.exe] "ul://<mode>/<path to file>?[varnameN=valueN[&...]][&_c=true]"`

### from the shell

`uploader[.exe] [--varname1=value1 [--varnameN=value=N [...]]] [-c] -m=<mode> <path to file>`

For details, see «Command line Arguments» below.

The file to upload must always be set as positional parameter 1. `-m=...`is always required and selects the desired configuration (see `uploader.ini`).

`.exe` is only requried on Windows. The Application can be built on Windows, Linux and OSX.

### as protocol handler

Register your custom protocol, you can use `example.reg` for windows. Follow the instructions in the comment before importing the reg file. `uploader.exe` must be called by the protocol as executable, and the complete url must be provided as the first argument. `uploader.exe` must be in the same fodler as `uploader.exe`.

Once you have your own protocol registered, say it's called `ul://` then you can run the program as follows in `cmd.exe`:

```cmd
start "" "ul://mode/path_to_file?param1=value1&param2=value2"
```

or you may use it in a html page like so: 
```html
<a href="ul://mode/path_to_file?param1=value1&param2=value2">Upload Stuff!</a>
```

You should be able to use the above url in any browser or program that uses the shell to execute custom protocols (default in windows and osx, most linux desktop environments).

### Command line Arguments

- `-c`: (optional) open console widget, this is for debugging purposes only (default: off)
- `-s`: (optional) path to uploader settings file (default: `<program-folder>/uploader.ini`)
- `-m=<mode-name>`: the mode parameter matches the `[mode-<mode-name>]` section in the `uploader.ini`
- `<file-name>`: must always be present except if `user-select = true` is set in the config section. If set the user gets a file selector in the gui.
- `--xx=nn`: (optional) all parameters starting with double-dash are passed into the program as variable to be used for payload-variable substitution. These parameters are all user-defined.

Example: `uploader.exe --fid=1234 --pid=5678 -m=select c:\temp\some-file.pdf`

### Payload

To send additional payload with the file, you have to configure the following
variables in the `uploader.ini` file:

```ini
; name of the form field for the payload
post-data-name = attributes

; file containing the payload with variables
; CAVE: path is relative to the folder where the configuration file is stored
post-data = with-param.payload

; (optional) content-type of the mime-part. Defaults to "text/plain"
post-data-content-type = application/json
```

### example payload file with-param.payload

```json
{
    "pid": "$pid",
    "fid": "$fid"
}
```

`$fid` and `$pid` in the payload will be substituted with the content of the
command-line arguments `--fid=...` and `--pid=...`.

### Files and Paths

`uploader` declares an environment variable called `$UPLOADER_HOME` which 
points to the executable's directory. Keep in mind, that this variable is only
avaialbe after startup of the executable and is intended to be used in the 
configuration. You are not able to use it for starting the application unless
you make sure it is system wide set through your deployement.

The positional parameter 1 «file to upload» is always relative to the 
environments working directory. Make sure to set this apropriately 
when launching the program or use absolute paths.

All other paths are relative to the programs execution directory or
configuration file regardless what `%CD%` or `cwd` says. The following files
are always relative to the programs execution directory.

#### relative to executable dicrectory

- `uploader.ini`: the programs configuration file, if `-s=` is not defined on the command line
- `*.payload`: payload files for http post data, if `-s=` is not defined on the command line
- `application-image` config value must point to a png/jpg file inside the program folder.

#### relative to configuration file

if `-s=...` is set on the command line, then the configuration file is loaded
from that location and `*.payload` files are read from the same folder as
the configuration file.

#### Log File

The program always writes a log file, the log file location is hardcoded and 
is set to the user's profile. Note that you can set a size limite for the 
log file with the configuration value `max-log-file-size` (default 500kb). 
If you set it to 0, the file will be created but all data will be cleared 
after successfull execution.

- Windows: %APPDATA%\..\local\uploader.exe\uploader.log
- Unix: ~/.local/share/uploader/uploader.log
- OSX: ~/Library/Application Support/uploader/uploader.log

## Configuration file uploader.ini

```
[uploader]
; uploader config file
;
; the command line argument `-m` will define which config section is used.
; If called with `-m=whatever` the application searches for a section
; called [mode-whatever]. if not found, the application exits.
;
; All configuration options set in the [uploader] section apply for all mode
; configurations (global configuration). They, however, can be overridden
; individually per section.

;; (optional) maximum log file size (bytes)
;
; the log file will automatically reduced in size if it gets larger than this
; size.
;
; NOTE: this configuration can only be sto in the [uploader] section,
;       no mode specific value possible.
;
; The log file location is system specific
; - Windows: %APPDATA%\..\local\uploader\uploader.log
; - Unix: ~/.local/share/uploader/uploader.log
; - OSX: ~/Library/Application Support/uploader/uploader.log
;
; default is 500'000 bytes
;max-log-file-size = 5000

;; general settings
; (optional) http connection timeout
; default is 3
timeout = 3

;; (Optional): (debugging) dump raw http communication to stdout
;
; default is false
;http-to-stdout = true

;; (optional) request headers
;
; default request headers, the headers defined here are applied to all 
; configurations below the configuration might override them.
;
; format: header-<header name> = <header value>
;
; Example:
; `header-Authorization = Basic: user|pass==`
;
; this will send the following over the wire:
; `Authorization: Basic: user|pass==\r\n`
header-Authorization = Basic: user|pass==
header-HE_USER = simonrulez

;; branding options
; all branding options except `application-title` may contain simmple
; HTML (such as <br>, <p>, <bold>, etc.)
;
; however: the whole value must be on a single line, linebreaks are
; not allowed within this file.
;
; The following regions of the app can be influenced with this config
;
; +---------------------------------+
; | ## (1) Window title          [X]|
; +----------+----------------------+
; |          | (3) App Header       |
; | (2)      | ###########_____ NN% |
; | App Logo | (4) Friendly text    |
; |          | (5) Error reason     |
; +----------+----------------------+
; | (6) Status bar                  |
; +---------------------------------+
;
; Config values
; - (1) Window title: `application-title`
; - (2) App Logo: `application-image`
; - (2) App Header: `application-header`
; - (4) Friendly text
;   - `application-text` initially
;   - `application-text-error` when an error happens
; - (5) Error reason: cannot be set by user
; - (6) Status bar: cannot be set by user
;

;; (optional) this is show in the titlebar and the taskbar
; default is "Uploader"
application-title = Uploader

;; (optional) window main title
;
; this is shown in the frame as title, most probably
; the user will refer to this name as the application name
;
; default is "HTTP Uploader"
application-header = PatArchive Uploader

;; (optional) Information text
;
; text displayed below the progress bar. you may use simple html such as HTML 3
;
; default is ""
application-text = <br>Your Document is being uploaded.<br><br>Just a moment please.

;; (optional) Information text on error (friendly error message)
;
; text displayed below the progress bar when an error happens. right above the error message.
;
; default is ""
application-text-error = <br><b>Something went Wrong</b><br><br><a href="http://google.com?q=Help+Desk">Help Desk</a>

;; (optional) copyright information
;
; optional, set the copyright line, you may use HTML 3. This text is displayed 
; below the `application-text`
;
; default is ""
application-copyright = © 2021, Simon Wunderlin <a href="https://www.gnu.org/licenses/old-licenses/lgpl-2.1-standalone.html">LGPL 2</a>

;; (optional) image left to te progress bar
;
; this image needs to be in a square shape, 256*256pixel wide. It can be a
; png, jpg or gif image.
;
; default is "", a compiled in resource will be used
application-image = uploader-logo.png

;; global post url
;
; this url will be used if the mode configuration does not define a url
;url = http://localhost/upload.php?sleep_after=3
url = http://localhost/upload.php?sleep_after=3

;; (optional) default mode
;
; if set, the -m=... command line argument is optional. if -m=... is not set
; the following mode is used.
;
; this is useful when you want to use drag&drop (osx only at the moment)
;
; default is ""
default-mode = with-param

[mode-upload]
; test with: uploader -m=upload path/to/some/file.txt
;
; this is a minimal upload example. We post a file and nothing more. 
; The file name is provided on the command line. After a successfull
; upload, the program wil close. If an error occours, the program
; will stay open and display an error message.

; post url
;url = http://192.168.0.176/upload.php

; multipart-form field name for the file
file-form-name = uploader1


[mode-with-param]
; test with: uploader -m=with-param --pid=1234 --fid=5678 path/to/some/file.txt
; 
; This example shows how to send extra json metadata together with the file 
; upload. 
;
; This example also shows, how the response of the upload can be parsed 
; and how values are extracted. The response needs to be of content-type
; application/json. Contents of the result json can be used to construct a 
; post upload command.

; post url
;url = http://192.168.0.176/upload.php?sleep_after=3

; multipart-form field name for the file
file-form-name = uploader1

;; (optional) minimal file size (bytes) allowed
;
; to prevent empty uploads we ca check the file size before uploading.
; typically, an empty zip file is 22bytes, an empty pdf file varies between
; 1117-4126 bytes
;
; default is 0
file-min-size = 25

;; (optional) file max size to upload (bytes)
;
; prevent upload of too large files.
;
; set to 0 to disable
;
; default is 0
file-max-size = 0

;; (optional) form field for uploading extra meta data
;
; `post-data` must be set together with this option.
post-data-name = attributes

;; (optional) payload file
;
; file name of the payload that will be sent in post-data-name
;
; relative paths is always relative to the config file.
; without the command line option `-s=...` this equals to the program folder.
; if a specific config file path is provided with `-s=...` the the payload
; file will also be sought in the folder where the config ini file resides.
;
; variables in this file will be replaced with the corresponding
; `--name=value` command line parameters
;
; `post-data-name` must not be empty for the payload to be sent
;
; content of file `with-param.payload`:
; {"pid": "$pid", "fid": "$fid", "unix_user": "$USER", "win_user": "$USERNAME"}
;
; $pid will be replaced with the value of the command line parameter
; `--pid=1234` and $fid will be replaced with the value of `--fid=5678`.
;
; the following payload will be sent:
; on windows:
; {"pid": "1234", "fid": "5678", "unix_user": "$USER", "win_user": "wunderlins"}
;
; on a posix system
; {"pid": "$pid", "fid": "$fid", "unix_user": "wunderlins", "win_user": "$USERNAME"}
;
; Environment variables will also be expanded in the payload string.
;
; If there is a payload variable with the same name as an environment variable,
; the payload variable (command line argument) will be used.
post-data = with-param.payload

;; (optional) tell the server what content type this part of the post has
post-data-content-type = application/json

;; (optional) extra request http header
;
; add an extra request header. this will result in the 
; following header line: `HE_USER: simonrulez-maybe? .... baby?\r\n`
header-HE_USER = simonrulez-maybe? .... baby?

;; (optional) parse the result json, extract some values from it
;
; json pointer (RFC6901) to the result element.
; must point to a scalar value or the parser will fail
;
; this will create a variable named `$host` containing the value
; of the json element `/ARRAY/key/1`
resultvar-host = /ARRAY/key/1

; (optional) run the following post upload command
;
; here we can use the variables that have been parsed from 
; the post's response. If the command starts with ^[a-zA-Z]+://
; then the shell will be used to execute the command. This will 
; typically open the default browser on your system if the url is
; an http:// url, or the file browser if it is an ftp:// url.
;command = http://google.com?q=host:+$host

; additionally, a system commadn can be started. If the string does 
; not start with a protocol, then a new process istarted and the 
; command below is executed. Variables can be used in the 
; command-line to launch the program like in urls.
; 
; if you wish th use environment variables, you may.
;
;command = "$windir\notepad.exe $host"

;; (opional) run a command before uploading
;
; this command is blocking, when it exits, uploader will fetch the
; file provided by the command line argument <file>
;
; if `pre-command` is producing a file you want to upload, you must make sure
; that the pre-command is storing the file in the location `uploader.exe`
; is looking for (command line argument <file>) or `uploader.exe` will exit
; with exit code 14: FILE_NOT_EXISTS.
;
; default is ""
;
;pre-command = xclock -bg yellow -strftime "%H %M" -digital
;pre-command = ../../resources/exit_err.sh

;; (optional) allowed exit codes of pre-command
;
; when launching pre-command, we check it's exit code. If it is not 0
; then we display an error.
;
; sometimes a user might legitimately cancel the `pre-command`, in that case we
; don't have anything to do nor display. If the program returns a unique
; exit code for this action, you may add it here to to ignore list and no
; error will be shown, uploader.exe will exit iwth exit code 0
; default is "";
;
pre-command-exit-codes = 255 254 -1 -2

[mode-select]
;; file selection example
; 
; test with: uploader -m=select
;
; this is a minimal example of a file selection dialog. The user 
; will have to choose the upload file manually.
;
; if a file is provided on the command line, it will be ignored.

; post url
;url = http://192.168.0.176/upload.php

; multipart-form field name for the file
file-form-name = uploader1

;; (optional) display file selection dialog
;
; show file chooser dialog, ignore command line argument <file>
; user has to choose file and hit upload manually.
;
; the command line argument <file> is not required when this option is set.
;
; default is false
user-select = true
```
# Exit / Error codes

When the program exits after an error, the following exit codes are returned (exit code 0 is success):

#### 1: CONFIG_NOT_READABLE

"Config file not readable"<br>
*file*: `uploader/main.cpp:114`

#### 3: NO_MODE

"Invalid mode, `-m=`."<br>
*file*: `uploader/main.cpp:142`

#### 4: ALREADY_RUNNING

"Process is already running."<br>
*file*: `uploader/main.cpp:109`

#### 10: SETTINGS_NOT_EXIST

"Config file does not exist."<br>
*file*: `uploader/main.cpp:151`

#### 11: SETTINGS_NO_MODE

"No mode provided, check `-m=...`."<br>
*file*: `uploader/main.cpp:154`

#### 12: SETTINGS_PAYLOAD_NOT_EXIST

"Payload file `post-data` not found."<br>
*file*: `uploader/main.cpp:157`

#### 13: SETTINGS_PAYLOAD_NOT_READABLE

"Payload file `post-data` is not readable."<br>
*file*: `uploader/main.cpp:160`

#### 14: SETTINGS_PARSE_ERROR

"Failed to parse config file."<br>
*file*: `uploader/main.cpp:163`

#### 15: SETTINGS_INVALID_MODE

("Mode '%1' is not configured.").arg(programArguments.mode)<br>
*file*: `uploader/main.cpp:166`

#### 16: SETTINGS_INVALID_CONFIGURATION

"Configuration error."<br>
*file*: `uploader/main.cpp:169`

#### 17: FAILED_PRECOMMAND

("pre-command process finished with invalid exit code: %1").arg(res)<br>
*file*: `uploader/main.cpp:188`

#### 20: FILE_PARAM_MISSING

"No file specified"<br>
*file*: `uploader/main.cpp:196`

#### 21: FILE_NOT_EXISTS

"File '"+programArguments.command_line_args.at(0)+"' does not exist"<br>
*file*: `uploader/main.cpp:199`

#### 22: FILE_NOT_READABLE

"File '"+programArguments.command_line_args.at(0)+"' is not readable"<br>
*file*: `uploader/main.cpp:202`

#### 23: FILE_TOO_SMALL

"File '"+programArguments.command_line_args.at(0)+"' is too small: " + number(s) + "bytes."<br>
*file*: `uploader/main.cpp:208`

#### 24: FILE_TOO_LARGE

"File '"+programArguments.command_line_args.at(0)+"' is too large: " + number(s) + "bytes."<br>
*file*: `uploader/main.cpp:214`

#### 30: NETWORK_FILE_READ

"Could not read upload file."<br>
*file*: `uploader/uploader.cpp:126`

#### 30: NETWORK_FILE_READ

"Upload interruped or otherwise failed."<br>
*file*: `uploader/uploader.cpp:132`

#### 31: NETWORK_STATUS_NOT_200

"HTTP Response Status not 200."<br>
*file*: `uploader/uploader.cpp:129`

#### 31: NETWORK_STATUS_NOT_200

"http status code: " + number(res.statusCode)<br>
*file*: `uploader/uploader.cpp:262`

#### 33: NETWORK_FAILED_TO_CONNECT

"Failed to connect to Server."<br>
*file*: `uploader/uploader.cpp:135`

#### 33: NETWORK_FAILED_TO_CONNECT

"Failed to connect to Server, reason: " + message<br>
*file*: `uploader/uploader.cpp:138`

#### 34: NETWORK_UNKNOWN

"General Network Error: " + number(errorCode)<br>
*file*: `uploader/uploader.cpp:141`

#### 34: NETWORK_UNKNOWN

"Http Communication error: " + res.errorMessage<br>
*file*: `uploader/uploader.cpp:257`

#### 35: NETWORK_WRONG_CONTENTTYPE

"Response contains no JSON: " + res.contentType<br>
*file*: `uploader/uploader.cpp:276`

#### 40: RESPONSE_PARSE_JSON

"Failed to parse response json, reason: " + varres.errorMessage<br>
*file*: `uploader/uploader.cpp:283`

#### 41: RESPONSE_VARIABLE_SUBSTITUTE

"failed to replace variables, reason: " + cmdres.errorMessage<br>
*file*: `uploader/uploader.cpp:290`

#### 50: URLPARSE_NO_MODE

"Missing mdoe in url: " + (argv[1])<br>
*file*: `uploader/main.cpp:134`

#### 51: URLPARSE_NO_FILE

"Missing file attribute in url: " + (argv[1])<br>
*file*: `uploader/main.cpp:131`

#### 52: URLPARSE_MALFORMED_URL

"Failed to parse url: " + (argv[1])<br>
*file*: `uploader/main.cpp:128`

#### 128: EXECUTE_CMD_EMPTY

("Empty pre-command: %1").arg(res)<br>
*file*: `uploader/main.cpp:177`

#### 129: EXECUTE_QUTE_ERROR

("Unbalanced Quotes in pre-command: %1").arg(res)<br>
*file*: `uploader/main.cpp:180`

#### 129: EXECUTE_QUTE_ERROR

"Failed to processs command."<br>
*file*: `uploader/uploader.cpp:316`

#### 130: EXECUTE_PROCESS_FAILED_TO_START

("Failed to launch pre-command: %1").arg(res)<br>
*file*: `uploader/main.cpp:183`

#### 131: EXECUTE_PROCESS_FINISHED_UNECPECTED

("pre-command process finished unexpected: %1").arg(res)<br>
*file*: `uploader/main.cpp:186`

#### 132: EXECUTE_COMMAND_FAILED_TO_START

"failed to execute command."<br>
*file*: `uploader/uploader.cpp:328`

