@echo off
setlocal enabledelayedexpansion

:: echo +======================================================================
:: echo ^| Setting up Build environment, removing *msys* from path

:: SET REMOVE_STR=msys
:: SET OUT=
:: for %%D in ("%PATH:;=";"%") do (
::     echo %%~D | findstr /i %REMOVE_STR% >nul && (
::         REM echo NO  %%~D
::     ) || (
::         :: echo YES %%~D
::         call :add_path %%D
::     )
:: )
:: SET PATH=%OUT:"=%

:: set your path to the desired mingw installation
:: this is not needed anymore for reelase builds. use the 
:: provided mingw toolset from Qt
REM set MGW_BIN=C:/msys64/mingw64/bin

:: set path to camke, install it with Qts maintenance tool
set CMAKE_BIN=C:/Qt/Tools/CMake_64/bin
set MINGW_BIN=C:/Qt/Tools/mingw810_64/bin

:: make sure the right Qt installation is used, 
:: explicitly set the Qt directory!
::set QT_DIR=C:/Qt/5.12.11/mingw73_64/lib/cmake/Qt5
set QT_DIR=C:/Qt/5.15.2/mingw81_64/lib/cmake/Qt5
:: set QT_DIR=C:/Qt/Static/5.12.10_mingw64_static_release/lib/cmake/Qt5
set QT_BIN=%QT_DIR%/../../../bin

:: add it at the beginning of $PATH
set PATH=%QT_BIN%;%CMAKE_BIN%;%MINGW_BIN%;%PATH%

:: force explicit executables, this will most likely fail, you'll have to set some other stuff too.
:: cmake.exe -LA -DWITH_GUI=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=g++.exe -DCMAKE_C_COMPILER=gcc.exe -DCMAKE_RC_COMPILER=windres.exe -G "MinGW Makefiles" ..

:: if rc generation fails due to mangled paths, try to worce cmake to be less clever by directly using windres.exe
::cmake.exe -DWITH_GUI=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_RC_COMPILER=windres.exe -G "MinGW Makefiles" ..

mkdir build
cd build
:: mkdir qt_deps

:: optimal build command
cmake.exe -DCMAKE_BUILD_TYPE:String=Release -G "MinGW Makefiles" ..
REM cmake.exe -DCMAKE_BUILD_TYPE:String=Release -DCMAKE_CXX_COMPILER="%MINGW_BIN%/g++.exe" -DCMAKE_C_COMPILER="%MINGW_BIN%/gcc.exe" ..
IF %ERRORLEVEL% NEQ 0 goto :exit

return

:: compile
:: mingw32-make.exe -j 12
mingw32-make.exe -j12 all
IF %ERRORLEVEL% NEQ 0 goto :exit

:: find Qt Dependencies dependencies
::windeployqt --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "uploader/uploader.exe"

:: build zip and nsis installer
cpack -G ZIP
IF %ERRORLEVEL% NEQ 0 goto :exit

cpack -G NSIS64
IF %ERRORLEVEL% NEQ 0 goto :exit

:exit
echo Failed to build, aborting.
cd ..

:add_path
    set temp=%~1
    if defined OUT (
        SET OUT=%OUT%;"%temp%"
    ) else (
        :: echo first
        SET OUT="%temp%"
    )