# Uploader Development

## Building

The GUI has been developed in `Qt Creator` however, for building it `cmake` and a valid Qt 5 
installation should suffice. Some build and documentation tools require mingw on Windows, I
recommend to use Msys2 with the following packages:

- git
- mingw64/mingw-w64-x86_64-gcc
- mingw64/mingw-w64-x86_64-make
- mingw64/mingw-w64-x86_64-cmake
- mingw64/mingw-w64-x86_64-curl
- mingw64/mingw-w64-x86_64-nsis
- discount (for building the documentation)

## Dependencies (Qt5 and libcurl) 

This package requires Qt5 to build, on debian `sudo apt-get install qt5-default` and on OSX `sudo port install qt-513` (or newer). 

Building `libcurl` will require additional libraries, `./configure` will tell you what is missing. However it is easier to use the shared library provided by your package manager.

**Debian**:

```bash
sudo apt install libcurl4-openssl-dev qt5-default
```

**OSX**

```bash
port install curl qt-513 # or Qt5 >= 5.13
```

**Windows** (Mingw64)

```bash
pacman -Ss mingw-w64-x86_64-curl mingw-w64-x86_64-qt5
```

**IMPORTANT**: you must make sure that in your build environment, the Qt mingw binaries take precendence in the `%PATH%` variable over the msys64/mingw64 binaries!

## uploader 

### linux / osx

```bash
cd build
cmake ..
make -j 12 # build
make test # optional, requires http server setup, see Testing
make package 
```

### Windows (Mingw64)

Check the contents of `mingw_build.bat`. You'll most likely want to adjust the path to `QT_DIR`. This is the folder where Qt5's `Qt5Config.cmake` resides. You have 2 options on windows for a Qt library installation:

1. Download the Qt SDK from http://www.qt.io/download (Opensource LGPL)
2. install with pacman via `MSYS2` package manager: `pacman -S mingw-w64-x86_64-qt5`

#### QT SDK Options
![qt libs](resources/images/qt-sdk.png)
![qt tools](resources/images/qt-tools.png)

[MSYS2](https://www.msys2.org/) is the easier route. Once installed install the above mentioned packages. However, this setup is very hard to setup for debugging therefore option 1 is preferred.

```bash
mingw_build.bat
```

genearted Packages are stored in `release/`.


The `package` target will build a binary package (archive) and platform dependanet installer (`.deb` on Debian, `OSX Bundle` for MacOS and an `NSIS64` installer for windows).


## Testing

Unit tests are provided in the `uplaodTest` Project. For the unit test to
work you will need a working configuration and a backend that accepts
multipart-form data. An example is provided as php 7 script under
`resources/upload.php`. Install it on any php server, make sure to have a
webserver writeable directory called `uploads/` (you can change the
location in the script).

You will have to set the appropriate url in `uploadTest/res/uploader.ini` and
change the tests urls in `uploadTest/uploadertest.cpp` to match the one set in
the ini file or you'll get an assertion error.

# Development Resources

## Links

### libcurl

- [lcurl C examples](https://curl.se/libcurl/c/example.html)
- [lcurl progress function](https://curl.se/libcurl/c/progressfunc.html)
- [lcurl http post](https://curl.se/libcurl/c/http-post.html)
- [lcurl http post multi part](https://curl.se/libcurl/c/multi-post.html)
- [lcurl read response body in c++](https://stackoverflow.com/questions/44994203/how-to-get-the-http-response-string-using-curl-in-c)
- [lcurl for windows mingw](https://stackoverflow.com/questions/33394461/how-to-statically-link-to-libcurl-in-qt-on-windows)
    
### qTest

- [qTest 101](http://imaginativethinking.ca/qtest-101-writing-unittests-qt-application/)
- [qTest implementation](http://blog.davidecoppola.com/2017/11/cpp-unit-testing-with-qt-test-introduction/)
- [qTest arc/argv](https://stackoverflow.com/questions/55550872/pass-argc-and-argv-to-qapplication-into-unit-test-case-method)
- [QSignalSpy - observe signals](https://doc.qt.io/qt-5/qsignalspy.html)

### json-path

- [c-json-path](https://github.com/arisi/c-json-path)
- [IETF JsonPath Spec](https://tools.ietf.org/id/draft-goessner-dispatch-jsonpath-00.html)
- [RFC6901 jsonpointer](https://datatracker.ietf.org/doc/html/rfc6901)
- [JSON for Modern C++ Version 2.0.0](https://github.com/nlohmann/json/releases/tag/v2.0.0)
- [json-c header only parser nicluding jsonpointer (RFC6901)](https://github.com/json-c/json-c)

### handling processes with Qt5

- [QProcess API](https://doc.qt.io/qt-5/qprocess.html#details)
- [QProcess::waitForStarted()](https://doc.qt.io/qt-5/qprocess.html#waitForStarted)
- [QProcess::waitForFinished()](https://doc.qt.io/qt-5/qprocess.html#waitForFinished)
- [QProcess::startDetached()](https://doc.qt.io/qt-5/qprocess.html#startDetached-1)

### w32 processes

- [check for process](https://stackoverflow.com/questions/29246124/check-if-process-is-running-windows)
- [C++, WinAPI?](https://stackoverflow.com/questions/13657455/how-to-know-if-a-process-is-running-in-windows-in-c-winapi)
- [W32 API CreateToolhelp32Snapshot](https://stackoverflow.com/questions/865152/how-can-i-get-a-process-handle-by-its-name-in-c)

## Snippets 

### windows check for running process

```C++
#include <cstdio>
#include <windows.h>
#include <tlhelp32.h>

int main( int, char *[] )
{
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (Process32First(snapshot, &entry) == TRUE)
    {
        while (Process32Next(snapshot, &entry) == TRUE)
        {
            if (stricmp(entry.szExeFile, "target.exe") == 0)
            {  
                HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);

                // Do stuff..

                CloseHandle(hProcess);
            }
        }
    }

    CloseHandle(snapshot);

    return 0;
}
```

### generate array with random data

    { echo -n '{ '; \
        hexdump -n 64 -e '4/1 "\\%02X, " 1 " "' /dev/random \
        | sed -e 's/, $//'; \
        echo ' }'; }
