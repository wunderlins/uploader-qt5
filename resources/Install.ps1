# 2021-06-10 KCz
# 2021-09-01 KCz, neue Version 1.0.1.0
# 2021-09-13 KCz, neue Version 1.0.2.0, Version wird von der "uploader.exe" geholt
# 2021-09-15 WUS, Feherlhandling eingebaut

$Source_Path = "\\usb\ict\Apps\Administrative Sources\App\Uploader"
$Target_Path = "$Env:ProgramFiles\Uploader"
$Prog_Version = (Get-Item "$Source_Path\uploader.exe").VersionInfo.FileVersion
$Log_File = "$Target_Path\install.log"

function Fail {
    param (
        [Parameter(Mandatory=$true, HelpMessage="The error message which is logged to install.log")]
        [string] $Message

        [Parameter(Mandatory=$true, HelpMessage="Script will exit with this exit code.")]
        [Int32] $ExitCode
    )

    Add-Content -Path "$Log_File" -Value $Message
    [Environment]::Exit($ExitCode)
}

# Evtl. laufenden Prozess anhalten
If(Get-Process | Where-Object {$_.Name -eq 'uploader'}) {
	Stop-Process -ProcessName 'uploader' -Force
    If (Test-Path "$Target_Path\uploader.exe") {
        Remove-Item -Path "$Target_Path\uploader.exe" -Force
        if(-Not $?) {
            # :sadface:, exe cannot be removed, therefore we cannot update, FAIL
            Fail -ExitCode 1 -Message "Failed to delete uploader.exe, probably in use"
        }
    }
}

# Wenn vorhanden: Alte Programmversion entfernen
If (Test-Path $Target_Path) {
    Remove-Item "$Target_Path" -Force -Recurse
    if(-Not $?) {
        # :sadface:, folder cannot be removed, therefore we cannot update, FAIL
        Fail -ExitCode 2 -Message "Failed to delete uploader folder, probably in use"
    }
}

# Neue Dateien vom Share kopieren
New-Item -Path $Target_Path -ItemType Directory
Copy-Item "$Source_Path\*" "$Env:ProgramFiles\Uploader" -Recurse -Force
if(-Not $?) {
    # :sadface:, folder cannot be copied, therefore we cannot update, FAIL
    Fail -ExitCode 3 -Message "Failed to copy uploader folder and/or files, reason unknown"
}

# Uninstall-Informationen hinterlegen, aber nur wenn der Copy-Job erfolgreich war (F�r Inventartools etc.)
# WUS: wenn man hier erfolg messen will, muss man version aus exe auslesen (PE32+ Header) und verlgeichen 
#      oder einen hash vom srouce file mit dem target file vergleichen (minimal version, optimal version w�re ein vergleich aller dll/exe hashes)
If (Get-FileHash "$Source_Path\uploader.exe" -Algorithm "SHA256" -eq Get-FileHash "$Target_Path\uploader.exe" -Algorithm "SHA256" ) {
	$InstallationDate = Get-Date -Format "yyyyMMdd"
	$UninstallRegPath = "HKLM:SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Uploader"
	New-Item -Path $UninstallRegPath -Force
	New-ItemProperty -Path $UninstallRegPath -Name "InstallDate" -PropertyType String -Value $InstallationDate -Force
	New-ItemProperty -Path $UninstallRegPath -Name "DisplayName" -PropertyType String -Value "Uploader" -Force
	New-ItemProperty -Path $UninstallRegPath -Name "Publisher" -PropertyType String -Value "USB, Simon Wunderlin" -Force
	New-ItemProperty -Path $UninstallRegPath -Name "DisplayVersion" -PropertyType String -Value "$Prog_Version" -Force
	New-ItemProperty -Path $UninstallRegPath -Name "DisplayIcon" -PropertyType String -Value "$Target_Path\uploader.exe" -Force
	New-ItemProperty -Path $UninstallRegPath -Name "EstimatedSize"  -PropertyType DWord -Value 50448 -Force
	New-ItemProperty -Path $UninstallRegPath -Name "NoRemove" -PropertyType DWord -Value 1 -Force
	New-ItemProperty -Path $UninstallRegPath -Name "NoRepair" -PropertyType DWord -Value 1 -Force
	New-ItemProperty -Path $UninstallRegPath -Name "InstallSCCM" -PropertyType DWord -Value 1 -Force
	New-ItemProperty -Path $UninstallRegPath -Name "UninstallString"  -PropertyType String -Value 'Dummy' -Force
} else{
    # :sadface:, folder cannot be copied, therefore we cannot update, FAIL
    Fail -ExitCode 4 -Message "Source and destination executable do not match, installation probably seriously screwed up."
}

# all fine, we were successfull, make sure to remove the log if there is any from previous attempts.
If (Test-Path "$Log_File") {
    Remove-Item "$Log_File" -Force -Recurse
}
