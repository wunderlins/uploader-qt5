# libcurl dependencies
win32 {
    LIBS += -L$$PWD/../curl-7.76.1/lib/.libs/ -llibcurl.dll -L$$PWD/../w32deps -lws2_32
    INCLUDEPATH += $$PWD/../curl-7.76.1/include $$PWD/w32deps
    DEPENDPATH += $$PWD/../curl-7.76.1/lib/.libs $$PWD/w32deps
} else {
    unix: LIBS += -L$$PWD/../curl-7.76.1/lib/.libs/ -lcurl
    INCLUDEPATH += $$PWD/../curl-7.76.1/include
    DEPENDPATH += $$PWD/../curl-7.76.1/include
    unix: PRE_TARGETDEPS += $$PWD/../curl-7.76.1/lib/.libs/libcurl.a
}
