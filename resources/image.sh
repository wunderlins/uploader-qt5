#!/usr/bin/env bash

echo -n '.flow { background:url(data:image/png;base64,' > image.css
base64.exe -w 0 uploader.png >> image.css
echo ') top left no-repeat; ); }' >> image.css
echo "<style>" >> README.html
cat image.css >> README.html
echo "</style>" >> README.html
sed -i -e 's,<p><img style="float: right" src="uploader.png"></p>,<div class="flow" style="float: right; width: 342px; height: 845px;"></div>,' README.html
