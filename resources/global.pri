# version
VERSION = "1.0.2"

DEFINES += VERSION="$${VERSION}"
DEFINES += GIT_VERSION=$$system(git describe --always --abbrev=0)

QMAKE_TARGET_COMPANY =
QMAKE_TARGET_DESCRIPTION = "HTTP File Uploader"
QMAKE_TARGET_COPYRIGHT = "2021 Simon Wunderlin"
QMAKE_TARGET_PRODUCT = Uploader

# disable debug/release sub directories in build on windows
win32 {
    # executable name
    DEFINES += NAME="uploader.exe"
    CONFIG -= debug_and_release debug_and_release_target
    QT += winextras
    RC_ICONS = ../resources/icon.ico
    RC_CODEPAGE = 1252
    # RC_LANG
} else {
    # executable name
    DEFINES += NAME="uploader"
}

# disable unused variable warnings
QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter

ICON = ../resources/images/uploader.icns
