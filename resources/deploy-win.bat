SET QTDIR=C:\Qt5\Qt5.12.4\5.12.4\mingw73_64
SET QTDIR=C:\Qt\5.15.2\mingw81_64
SET EXE=build\uploader\uploader.exe
SET DST=release\
mkdir %DST%
mkdir %DST%\ProgramFiles\
:: mkdir %DST%\ScanClient\
mkdir %DST%\eDocPrintPro\
%QTDIR%\bin\windeployqt --dir %DST%\ProgramFiles\ --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw %EXE%
copy curl-7.76.1\lib\.libs\libcurl-4.dll %DST%\ProgramFiles\
copy resources\uploader.ini %DST%\ProgramFiles\
copy w32deps\distdll\*.dll %DST%\ProgramFiles\
copy %EXE% %DST%\ProgramFiles\
copy resources\README.html %DST%\
copy resources\example.reg %DST%\

:: example ini files
copy resources\example_uploader_minimal.ini %DST%\
copy resources\example_uploader_postrun.ini %DST%\
copy resources\example_uploader_select.ini %DST%\

:: USB Files
copy print2he.bat %DST%\eDocPrintPro\
:: copy scan2he.bat %DST%\ScanClient\
del %DST%\ProgramFiles\uploader.ini
del %DST%\ProgramFiles\*.payload
copy he.ini %DST%\ProgramFiles\uploader.ini
copy i-engine.ini %DST%\eDocPrintPro\
copy i-engine-kons.ini %DST%\eDocPrintPro\
copy scan2he.payload %DST%\ProgramFiles\
