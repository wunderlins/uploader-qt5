<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ($_SERVER['REQUEST_METHOD'] === 'GET') { ?>
<form method="POST" enctype="multipart/form-data" action="upload.php">
	<input type="file" name="uploader1">
	<input type="text" name="attributes" value="attr1">
	<input type="submit">
</form>


<?php exit(0); }

if (isset($_GET["sleep"])) {
    sleep($_GET["sleep"]);
    exit(0);
}

$currentDirectory = getcwd();
$uploadDirectory = "/uploads/";

$errors = []; // Store errors here

$fileName = $_FILES['uploader1']['name'];
$fileTmpName  = $_FILES['uploader1']['tmp_name'];
$uploadPath = $currentDirectory . $uploadDirectory . basename($fileName); 

if (empty($errors)) {
    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

    if (!$didUpload) {
        echo "An error occurred. Please contact the administrator.";
        exit(1);
    }
} else {
    foreach ($errors as $error) {
        echo $error . "These are the errors" . "\n";
        exit(1);
    }
}

if (isset($_GET["sleep_after"])) {
    sleep($_GET["sleep_after"]);
}

// decode attributes
foreach ($_POST as $key => $value) {
	$_POST[$key] = json_decode($value);
}


$ret = array(
    "POST" => $_POST, 
    "FILES" => $_FILES, 
    "HEADERS" => getallheaders(),
    "ARRAY" => array("key" => array(0,1,2))
);

// write rawresponse to file
file_put_contents($currentDirectory . $uploadDirectory . "/response.payload", json_encode($ret));
foreach($_POST as $key => $value) {
    file_put_contents($currentDirectory . $uploadDirectory . "/response.payload.$key", $value);
}

header('Content-Type: application/json');
echo json_encode($ret);

?>
