#!/usr/bin/env bash

FILES="uploader/main.cpp uploader/uploader.cpp"
OUT_MSG=resources/error_messages.csv
OUT_NUM=resources/error_enum.txt
OUT=resources/error_catalog.csv
OUT_MD=resources/errors.md
OUT_CODES=resources/error_codes.txt
#echo "File;Line;ErrorCode;Message" > $OUT_MSG

# fidn all handle_error() calls in code
grep -n "handle_error" $FILES | grep -v "//" | grep -v 'int Uploader::handle_error' | \
    sed -e 's,:\([0-9]\+\):[ \t]*,;\1;,; s,Uploader::handle_error(,,; s,UploaderLib::,,; s,);$,,; s,[\t ]*return[\t ]*,,; s/\([A-Z0-9]\+\), */\1;/; s,QString::,,g; s/QString//g' \
    > $OUT_MSG

sed -i -e 's/[^\r]\n/\r\n/gm' $OUT_MSG

awk '/OK = 0/,/}/ {print $1}' uploaderLib/uploaderlib.h | grep -v '}' | grep -v '//' | grep -v '^$' | sed -e 's/,.*//' > $OUT_CODES

# generate an code to name mapping table from error enum
counter=0
echo -en "" > $OUT_NUM
while read l; do
    if [[ "$l" == "" ]]; then continue; fi

    if [[ `echo "$l" | grep '='` ]]; then
        p1=`echo $l | cut -d '=' -f1`
        p2=`echo $l | cut -d '=' -f2`
        counter=$p2
        l=$p1
    fi

    echo "$counter;$l" >> $OUT_NUM
    counter=$((counter + 1))
done < $OUT_CODES

# generate a csv file that complements messages with error codes
while read l; do
    #echo $l
    name=`echo $l | cut -d ';' -f3`
    #echo $name
    code=`grep ";$name$" $OUT_NUM | cut -d';' -f1`

    echo -en "$code;"
    echo "$l"

done < $OUT_MSG | sort -t ';' -k 1 -g | uniq > $OUT

# ok, now we can build the md file
echo -en "" > $OUT_MD # truncate
while read l; do
    # pos 1, error code
    # pos 2: file
    # pos 3: line
    # pos 4: error name
    # pos 5: message

    code=`echo $l | cut -d';' -f1`
    file=`echo $l | cut -d';' -f2`
    line=`echo $l | cut -d';' -f3`
    name=`echo $l | cut -d';' -f4`
    msg=`echo $l | cut -d';' -f5`

    echo "#### $code: $name" >> $OUT_MD
    echo -e "" >> $OUT_MD
    echo "$msg<br>" >> $OUT_MD
    echo -n '*file*: `' >> $OUT_MD
    echo -n "$file:$line" >> $OUT_MD
    echo '`' >> $OUT_MD
    echo -e "" >> $OUT_MD

done < $OUT

# generate complete readme
cat resources/README_template.md > README.md 
cat $OUT_MD >> README.md

# create README.html
markdown -f +autolink -f +githubtags -f+strikethrough -f +superscript -f +tables -f +fencedcode -f +image -f +header -f +toc -o resources/README_tmp.html README.md

echo -en "<!doctype html>\n\n<html><head><title>Uploader</title><style>\n" > resources/README.html
cat resources/github.css >> resources/README.html
echo -en "\n</style></head><body>\n" >> resources/README.html
cat resources/README_tmp.html >> resources/README.html
echo -en "\n</body>\n</html>\n" >> resources/README.html

# cleanup temp files
rm $OUT_MSG $OUT_NUM $OUT_MD $OUT_CODES resources/README_tmp.html

# inline image flow image
echo -n '.flow { background:url(data:image/png;base64,' > resources/image.css
base64 -w 0 resources/uploader.png >> resources/image.css
echo ') top left no-repeat; ); }' >> resources/image.css
echo "<style>" >> resources/README.html
cat resources/image.css >> resources/README.html
echo "</style>" >> resources/README.html
sed -i -e 's,<p><img style="float: right" src="uploader.png"></p>,<div class="flow" style="float: right; width: 405px; height: 947px;"></div>,' resources/README.html
sed -i -e 's,uploader.png,resources/uploader.png,' README.md

# inline the rest of the images in the html file
html_file=resources/README.html
html=$(cat $html_file)
while read line; do
    # extract <img src="..."
    tag=$(echo "$line" | sed -e 's/.*<img src=/<img src=/; s/["'\''][ \t].*/" /')
    
    #extract file name
    file=$(echo $tag | sed -e 's/.*=["'\'']//; s/["'\'']//;')
    echo $file

    # base 64 encode
    #base64 -w 0 $file >> $file.base64
    base64=$(base64 -w 0 $file)
    #delete_later="$delete_later $file.base64"

    # replace image 
    #sed -e "s|"$file"|data:image/png;base64,$(cat $file.base64)|" $html_file
    html=$(echo "${html//$file/data:image/png;base64,$base64}")

done <<<$(grep '<img src="resources/' $html_file)
echo "$html" > $html_file