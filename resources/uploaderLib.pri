 
# uploader lib
unix|win32: LIBS += -L$$OUT_PWD/../uploaderLib/ -luploaderLib

INCLUDEPATH += $$PWD/../uploaderLib
DEPENDPATH += $$PWD/../uploaderLib

win32:!win32-g++: PRE_TARGETDEPS += $$OUT_PWD/../uploaderLib/uploaderLib.lib
else:unix|win32-g++: PRE_TARGETDEPS += $$OUT_PWD/../uploaderLib/libuploaderLib.a
