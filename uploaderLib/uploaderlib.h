#ifndef UPLOADERLIB_H
#define UPLOADERLIB_H

#include <QObject>
#include <QString>
#include <QHash>
#include <QRegularExpression>
#include <QFileInfo>
#include <QProcess>
#include <QUrl>
#include <QUrlQuery>

#include "ini.h"
#include "globals.h"
#include "uploadthread.h"
#include "json.hpp"

using json = nlohmann::json;

typedef struct jsonResultVar_t {
  QHash<QString, QString> variables;
  int error;
  QString errorMessage;
} jsonResultVar_t;

typedef struct jsonResult_t {
  QString value;
  int error;
  QString errorMessage;
} jsonResult_t;

typedef jsonResult_t varSubtitution_t;

typedef struct httpResponse_t {
  int statusCode;
  QString contentType;
  int contentLength;
  QString body;
  QHash<QString, QString> header;
  int error;
  QString errorMessage;
} httpResponse_t;

class UploaderLib: public QObject
{
    Q_OBJECT

public:
    enum UploaderLibError {
        OK = 0,
        CONFIG_NOT_READABLE,
        SHOW_HELP,
        NO_MODE,
        ALREADY_RUNNING,

        SETTINGS_NOT_EXIST=10,
        SETTINGS_NO_MODE,
        SETTINGS_PAYLOAD_NOT_EXIST,
        SETTINGS_PAYLOAD_NOT_READABLE,
        SETTINGS_PARSE_ERROR,
        SETTINGS_INVALID_MODE,
        SETTINGS_INVALID_CONFIGURATION,
        FAILED_PRECOMMAND,

        FILE_PARAM_MISSING=20,
        FILE_NOT_EXISTS,
        FILE_NOT_READABLE,
        FILE_TOO_SMALL,
        FILE_TOO_LARGE,

        NETWORK_FILE_READ=30,
        NETWORK_STATUS_NOT_200,
        NETWORK_INTERRUPTED,
        NETWORK_FAILED_TO_CONNECT,
        NETWORK_UNKNOWN,
        NETWORK_WRONG_CONTENTTYPE,

        RESPONSE_PARSE_JSON=40,
        RESPONSE_VARIABLE_SUBSTITUTE,

        URLPARSE_NO_MODE=50,
        URLPARSE_NO_FILE,
        URLPARSE_MALFORMED_URL,

        // execute exit codes are intentionally above 127 to hopefully
        // not clash with the programms error codes
        EXECUTE_CMD_EMPTY=128,
        EXECUTE_QUTE_ERROR,
        EXECUTE_PROCESS_FAILED_TO_START,
        EXECUTE_PROCESS_FINISHED_UNECPECTED,
        EXECUTE_COMMAND_FAILED_TO_START,
    };

    UploaderLib(QObject *parent = nullptr);
    static QString preparePayload(QString postTemplate, QHash<QString, QString> variables);

    static UploaderLibError applicationInit(QString executableDirectory);
    static QHash<QString, QString> getConfigVarList(IniParser *ini, QString prefix, QString mode);
    static QVariant getConfigVar(IniParser *ini, QString name, QString mode, QVariant defaultval = QVariant());
    static QStringList parseOptions(QStringList arguments);
    static UploaderLibError parseCommandLineArguments(QStringList arguments);
    static UploaderLibError readSettings();
    static UploaderLibError fileIsReadable();
    static QString expandEnvironmentVariables(QString path);
    static QStringList splitCommandLine(QString commandline);

    static httpResponse_t parseHtttpResponse(QString response);
    static jsonResult_t extractFromJson(QString jsonContent, QString jsonPointer);
    static jsonResultVar_t findResultVars(QHash<QString, QString> result_var, QString httpBody);
    static varSubtitution_t substituteVariables(QHash<QString, QString> variables, QString text);

    static UploaderLibError preExecute(QString commandline);

    static UploaderLibError parseUrl(QString url);

    static const QString m_allowedVarChar;

    static void pruneLogFile();

signals:
    //void shutdown(int eixt_code);
    //void log(const QString message, bool section = false);

};

#endif // UPLOADERLIB_H
