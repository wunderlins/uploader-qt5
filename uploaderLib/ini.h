/**
 * ini parser
 * 
 * 2020, Simon Wunderlin
 */

#ifndef _INI_H_
#define _INI_H_

// set for testing
//#ifndef __cplusplus
//#define __cplusplus
//#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>

#ifdef __cplusplus 

#include <string>
#include <list>
using namespace std;

extern "C" {
#endif

/**
 * we are dynamically pre-allocating child items so we don't have
 * to call realloc() on every item added. If you have large structures
 * with many items settin this to a higher value might benefit 
 * performance (less realloc calls).
 * 
 * default: 10
 */
#define ALLOC_NUM_ITEMS 10

/**
 * This structure holds the last known position of element boundaries
 */
typedef struct {
	size_t section_start;
	size_t section_end;
	size_t item_start;
	size_t item_end;
	size_t item_equal;
	size_t item_comment;
} last_pos_t;

/**
 * This structure defines a key/value pair of an ini file
 */
typedef struct {
	int start; // start position in file
	int end;   // end position in file
	char* name;
	char* value;
} ini_item_t;

/**
 * this structure holds a section with N items 
 */
typedef struct {
	char* name;
	int length;  // number of items in this section
	int size;    // max allocated items
	ini_item_t **items;
} ini_section_t;

/**
 * Top level strcture, holds N sections.
 */
typedef struct {
	int length;   // number of sections
	int size;     // max allocated space in sections 
	ini_section_t **sections;
} ini_section_list_t;

// constructor and destructor
ini_section_list_t *ini_parse(FILE *fp);
void ini_free(ini_section_list_t *ini);

// find sections and keys in ini structure
ini_section_t *ini_find_section(ini_section_list_t *ini, 
                                const char* section_name);
ini_item_t *ini_find_key(ini_section_t *s, const char* key);

// get a value for a specific item in a section.
// returns NULL if not found
char *ini_get_value(ini_section_list_t *ini,
                const char* section, const char* key);

#ifdef __cplusplus 
} // end extern "C"

// C++ implementation

class IniParser {
protected:

public:
    // base variables
    int errnum = 0;
    char *inifile;
    FILE *fp;
    ini_section_list_t *section_list;


    IniParser();
    //IniParser(string file);
    ~IniParser();

    //int open();
    int open(string file);

    // lookup functions
	bool exists(const string section, const string key);
    string get(const string section, const string key, string default_value = "");
    list<string> sections();
    list<string> keys(const string section_name);
};

#endif

#endif // _INI_H_
