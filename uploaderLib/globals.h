#ifndef GLOBALS_H
#define GLOBALS_H

#define _STR(x) #x
#define STRINGIFY(x)  _STR(x)

#ifndef VERSION
    #define VERSION "1.0.0";
#endif
#ifndef GIT_VERSION
    #define GIT_VERSION
#endif
#if defined(Q_OS_WIN)
    #ifndef NAME
        #define NAME "uploader.exe";
    #endif
#else
    #ifndef NAME
        #define NAME "uploader";
    #endif
#endif

#include <QString>
#include <QFile>
#include <QHash>
//#include <QSettings>
#include <QCommandLineParser>
#include <QDebug>
#include "ini.h"

typedef struct programArguments_t {
    QFile log_file;
    bool log_file_open;

    QString mode;
    QFile file;
    QHash<QString, QString> parameter;
    QFile exe_dir;
    bool show_console;
//    QSettings *settings;
    QString config_file;
    QString post_data_file;
    QStringList command_line_args;
    bool dragndrop_file;
} programArguments_t;

typedef struct config_t {
    int timeout;
    int max_log_file_size;
    bool http_to_stdout;
    QHash<QString, QString> header;
    bool user_select;

    QString url;
    QString file_form_name;
    int file_min_size;
    int file_max_size;
    QString post_data_name;
    QString post_data;
    QString post_data_content_type;

    QHash<QString, QString> result_var;
    QString command;

    // branding
    QString application_title;
    QString application_header;
    QString application_copyright;
    QString application_text;
    QString application_text_error;
    QString application_image;

    QString pre_command;
    QString default_mode;
    QList<int> pre_command_exit_codes;
} config_t;

/*
enum argumentParserStatus {
    OK = 0,
    SHOW_HELP,
    ERROR_NO_MODE,
};
*/

/*
extern "C" typedef struct inihandle_t {
    char *inifile;
    int errnum = 0;
    FILE *fp;
} inihandle_t;
extern "C" inihandle_t inihandle;
*/

extern programArguments_t programArguments;
extern config_t config;
extern QVector<QString> availableModes;

#endif // GLOBALS_H
