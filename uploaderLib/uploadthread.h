#ifndef UPLOADTHREAD_H
#define UPLOADTHREAD_H

#include <curl/curl.h>
#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <QHash>
#include <QDebug>
#include <QMimeDatabase>
#include "globals.h"

typedef enum uploadError {
    UPLOAD_OK = 0,
    UPLOAD_FILE_NOT_READABLE,
    UPLOAD_STATUS_CODE_NOT_200,
    UPLOAD_MULTI_HANDLE_ERROR,
    UPLOAD_FAILED_TO_CONNECT,
    UPLOAD_CURL_ERROR
} uploadError;

class UploadThread : public QThread
{
    Q_OBJECT

public:
    explicit UploadThread(QObject *parent = nullptr);
    ~UploadThread();

    void upload(const QString &fileName, int waitTimeout, const QString payload, const QHash<QString, QString> header);

signals:
    void progressChanged(const int progress);
    void log(const QString message, bool section = false);
    void httpResponse(QString responseHeaderAndBody);
    void uploadError(int errorCode, QString message = "");
    void httpStatusCode(long httpStatusCode);

private:
    //static UploadThread * instance;
    void run() override;

    static int xferinfo(void *p,
                        curl_off_t dltotal, curl_off_t dlnow,
                        curl_off_t ultotal, curl_off_t ulnow);

    int xferinfo_static(void *p,
                        curl_off_t dltotal, curl_off_t dlnow,
                        curl_off_t ultotal, curl_off_t ulnow);
    QMutex m_mutex;
    QWaitCondition m_cond;
    bool m_quit = false;
    int m_waitTimeout = 0;

    QString m_fileName;
    QString m_payload;
    QHash<QString, QString> m_header;

};

//UploadThread* UploadThread::instance{nullptr};

#endif // UPLOADTHREAD_H
