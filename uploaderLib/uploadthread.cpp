#include "uploadthread.h"
#include <QTime>
#include <stdio.h>

// static members
//UploadThread* UploadThread::instance{nullptr};

/* In libcurl 7.61.0, support was added for extracting the time in plain
   microseconds. Older libcurl versions are stuck in using 'double' for this
   information so we complicate this example a bit by supporting either
   approach. */
#define TIME_IN_US 1 /* microseconds */
#define TIMETYPE curl_off_t
#define TIMEOPT CURLINFO_TOTAL_TIME_T
#define MINIMAL_PROGRESS_FUNCTIONALITY_INTERVAL     3000000
#define STOP_DOWNLOAD_AFTER_THIS_MANY_BYTES         3000000 // 3mb

struct myprogress {
  TIMETYPE lastruntime; /* type depends on version, see above */
  CURL *curl;
  UploadThread *thread;
};

// class implementation
UploadThread::UploadThread(QObject *parent) :
    QThread(parent),
    m_header(QHash<QString, QString>())
{
    // this is a static that always points to the last created instacne
    // beware, this is not thread safe, if multiple Uploader Objects are used
    // change this to a hashtable and save it by instance id
    //instance = this; // new solution, append thread to myprogress

    //connect(&parent, &UploadThread::httpResponse, this, &Uploader::httpResponse);
}

UploadThread::~UploadThread()
{
    m_mutex.lock();
    m_quit = true;
    m_cond.wakeOne();
    m_mutex.unlock();
    wait();
}

/* this is how the CURLOPT_XFERINFOFUNCTION callback works */
int UploadThread::xferinfo(void *p,
                    curl_off_t dltotal, curl_off_t dlnow,
                    curl_off_t ultotal, curl_off_t ulnow) {
    struct myprogress *myp = (struct myprogress *)p;
    CURL *curl = myp->curl;
    TIMETYPE curtime = 0;
    UploadThread *thread = myp->thread;

    curl_easy_getinfo(curl, TIMEOPT, &curtime);

    /* under certain circumstances it may be desirable for certain functionality
     to only run every N seconds, in order to do this the transaction time can
     be used */
    if((curtime - myp->lastruntime) >= MINIMAL_PROGRESS_FUNCTIONALITY_INTERVAL) {
    myp->lastruntime = curtime;

    /*
    #ifdef TIME_IN_US
        fprintf(stderr, "TOTAL TIME: %" CURL_FORMAT_CURL_OFF_T ".%06ld\r\n",
                (curtime / 1000000), (long)((int)curtime % 1000000));
    #else
        fprintf(stderr, "TOTAL TIME: %f \r\n", curtime);
    #endif
    */
    }

    /*
    fprintf(stderr, "UP: %" CURL_FORMAT_CURL_OFF_T " of %" CURL_FORMAT_CURL_OFF_T
          "  DOWN: %" CURL_FORMAT_CURL_OFF_T " of %" CURL_FORMAT_CURL_OFF_T
          "\r\n",
          ulnow, ultotal, dlnow, dltotal);
    */

    if (ulnow == 0)
        thread->progressChanged(0);
    else
        thread->progressChanged(ulnow * 100 / ultotal);

    if(dlnow > STOP_DOWNLOAD_AFTER_THIS_MANY_BYTES)
        return 1;

    return 0;
}

size_t write_callback(char *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

size_t header_callback(char* b, size_t size, size_t nitems, void *userdata) {
    int numbytes = (int) size * nitems;
    //printf("header_callback %.*d\n", numbytes);
    return numbytes;
}

void UploadThread::run() {
    /*
    qInfo() << "Starting upload thread with file:";
    qInfo() << m_fileName;
    qInfo() << "Payload " << m_payload;
    qInfo() << "Header: " << m_header;
    */

    emit log("UploadThread::run() -> " + config.url);

    // get mime type of the file
    QMimeDatabase db;
    QMimeType type = db.mimeTypeForFile(m_fileName);
    //qDebug() << "Mime type:" << type.name();

    CURL *curl;

    CURLM *multi_handle;
    int still_running = 0;

    curl_mime *form = NULL;
    curl_mimepart *field = NULL;
    struct curl_slist *headerlist = NULL;
    static const char buf[] = ""; // "Expect:";
    std::string readBuffer;

    curl = curl_easy_init();
    multi_handle = curl_multi_init();

    // failed to initialize curl
    if(!curl || !multi_handle) {
        emit log("curl_easy_init() failed");
        return;
    }

    // set network timeout
    curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, m_waitTimeout);

    /* Add custom headers */
    QHash<QString, QString>::const_iterator i = config.header.constBegin();
    while (i != config.header.constEnd()) {
        QString header(i.key() + ": " + i.value());
        headerlist = curl_slist_append(headerlist, header.toStdString().c_str());
        //qDebug() << "Added header: " << header;
        ++i;
    }

    // setup progress callback
    struct myprogress prog;
    prog.lastruntime = 0;
    prog.curl = curl;
    prog.thread = this;

    // disable CA check
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

    /* xferinfo was introduced in 7.32.0, no earlier libcurl versions will
       compile as they won't have the symbols around.

       If built with a newer libcurl, but running with an older libcurl:
       curl_easy_setopt() will fail in run-time trying to set the new
       callback, making the older callback get used.

       New libcurls will prefer the new callback and instead use that one even
       if both callbacks are set. */

    curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, xferinfo);
    /* pass the struct pointer into the xferinfo function, note that this is
       an alias to CURLOPT_PROGRESSDATA */
    curl_easy_setopt(curl, CURLOPT_XFERINFODATA, &prog);

    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);

    // capture response date
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
    //curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_callback);

    /* Create the form */
    form = curl_mime_init(curl);

    /* Fill in the file upload field */
    field = curl_mime_addpart(form);
    curl_mime_name(field, config.file_form_name.toStdString().c_str());
    curl_mime_filedata(field, m_fileName.toStdString().c_str());
    curl_mime_type(field, type.name().toStdString().c_str());

    /* Fill in the filename field */
    if (m_payload != "" && config.post_data_name != "") {
        field = curl_mime_addpart(form);
        curl_mime_name(field, config.post_data_name.toStdString().c_str());
        //printf("%s\n", m_payload.toStdString().c_str());
        curl_mime_data(field, m_payload.toStdString().c_str(), CURL_ZERO_TERMINATED);
        curl_mime_type(field, config.post_data_content_type.toStdString().c_str());
    }

    /* initialize custom header list (stating that Expect: 100-continue is not
     wanted */
    headerlist = curl_slist_append(headerlist, buf);

    /* what URL that receives this POST */
    curl_easy_setopt(curl, CURLOPT_URL, config.url.toStdString().c_str());
    if (config.http_to_stdout)
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    curl_easy_setopt(curl, CURLOPT_HEADER, 1L);

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
    curl_easy_setopt(curl, CURLOPT_MIMEPOST, form);

    curl_multi_add_handle(multi_handle, curl);
    CURLMcode mret = curl_multi_perform(multi_handle, &still_running);

    int rc = -1; /* select() return code */
    while(still_running) {
        struct timeval timeout;
        CURLMcode mc; /* curl_multi_fdset() return code */

        fd_set fdread;
        fd_set fdwrite;
        fd_set fdexcep;
        int maxfd = -1;

        long curl_timeo = -1;

        FD_ZERO(&fdread);
        FD_ZERO(&fdwrite);
        FD_ZERO(&fdexcep);

        /* set a suitable timeout to play around with */
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        curl_multi_timeout(multi_handle, &curl_timeo);
        if(curl_timeo >= 0) {
            timeout.tv_sec = curl_timeo / 1000;
            if(timeout.tv_sec > 1)
                timeout.tv_sec = 1;
            else
                timeout.tv_usec = (curl_timeo % 1000) * 1000;
        }

        /* get file descriptors from the transfers */
        mc = curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd);

        if(mc != CURLM_OK) {
            emit log("curl_multi_fdset() failed, code " + QString::number(mc));
            fprintf(stderr, "curl_multi_fdset() failed, code %d.\n", mc);
            break;
        }

        /* On success the value of maxfd is guaranteed to be >= -1. We call
           select(maxfd + 1, ...); specially in case of (maxfd == -1) there are
           no fds ready yet so we call select(0, ...) --or Sleep() on Windows--
           to sleep 100ms, which is the minimum suggested value in the
           curl_multi_fdset() doc. */

        if(maxfd == -1) {
    #ifdef _WIN32
            Sleep(100);
            rc = 0;
    #else
            /* Portable sleep for platforms other than Windows. */
            struct timeval wait = { 0, 100 * 1000 }; /* 100ms */
            rc = select(0, NULL, NULL, NULL, &wait);
    #endif
        } else {
            /* Note that on some platforms 'timeout' may be modified by select().
             If you need access to the original value save a copy beforehand. */
            rc = select(maxfd + 1, &fdread, &fdwrite, &fdexcep, &timeout);
        }

        switch(rc) {
            case -1:
                /* select error */
                break;
            case 0:
            default:
                /* timeout or readable/writable sockets */
                //printf("perform!\n");
                curl_multi_perform(multi_handle, &still_running);
                //printf("running: %d!\n", still_running);
                break;
        }
    }

    // check for general transmission errors of all curl_easy handles
    // if we detect one, we assume the transaction has failed and abort.
    //
    // see: https://curl.se/libcurl/c/curl_multi_info_read.html
    struct CURLMsg *m;
    do {
      int msgq = 0;
      m = curl_multi_info_read(multi_handle, &msgq);
      if(m && (m->msg == CURLMSG_DONE)) {
        //CURL *e = m->easy_handle;
        //qDebug() << "result: " << m->data.result;
        if (CURLE_OK != m->data.result) {
            // we have an error
            //qDebug() << "error: " << m->data.result;
            // https://curl.se/libcurl/c/curl_easy_strerror.html
            const char *errstr = curl_easy_strerror(m->data.result);
            emit uploadError(UPLOAD_CURL_ERROR, QString::number(m->data.result) + " " + QString(errstr));

            // free stuff
            curl_multi_cleanup(multi_handle);
            curl_easy_cleanup(curl);
            curl_mime_free(form);
            curl_slist_free_all(headerlist);

            return;
        }
        //curl_multi_remove_handle(multi_handle, e);
        //curl_easy_cleanup(e);
      }
    } while(m);

    // first of all, check the exit code of the multi handle
    //fprintf(stdout, "multi_handle return: %d, rc: %d\n", mret, rc);
    if (mret > 0) {
        emit uploadError(UPLOAD_MULTI_HANDLE_ERROR);
        return;
    }

    // get HTTP status code
    long http_status_code = 0;
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_status_code);
    emit log("Http status code: " + QString::number(http_status_code));
    emit httpStatusCode(http_status_code);

    // reset progress on error response
    if (rc == -1) {
        emit progressChanged(0);
        emit uploadError(UPLOAD_FAILED_TO_CONNECT);
    } else if (http_status_code != 200) {
        emit progressChanged(0);
        emit uploadError(UPLOAD_STATUS_CODE_NOT_200);
    } else {
        emit uploadError(UPLOAD_OK);
    }

    // log response data
    QString response = QString::fromStdString(readBuffer);
    emit httpResponse(response);

    // free stuff
    curl_multi_cleanup(multi_handle);

    /* always cleanup */
    curl_easy_cleanup(curl);

    /* then cleanup the form */
    curl_mime_free(form);

    /* free slist */
    curl_slist_free_all(headerlist);

    emit log("Finished uploading " + m_fileName);
}

void UploadThread::upload(const QString &fileName, int waitTimeout, const QString payload, const QHash<QString, QString> header) {

    emit log("Starting uploadthread.upload()", true);
    //qDebug() << "fileName: " << fileName;

    // check if file is readable
    QFile uploadFile(fileName);
    if (!uploadFile.exists()) {
        emit uploadError(UPLOAD_FILE_NOT_READABLE);
        return;
    }

    if (!uploadFile.open(QIODevice::ReadOnly)) {
        emit uploadError(UPLOAD_FILE_NOT_READABLE);
        return;
    }

    programArguments.file.close();

    const QMutexLocker locker(&m_mutex);
    m_fileName = fileName;
    m_waitTimeout = waitTimeout;
    m_payload = payload;
    m_header = header;

    if (!isRunning())
        start();
    else
        m_cond.wakeOne();
}
