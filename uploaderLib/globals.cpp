#include "globals.h"

programArguments_t programArguments {
    .log_file = {},
    .log_file_open = false,
    .mode = "",
    .file = {},
    .parameter = QHash<QString, QString>(),
    .exe_dir = {},
    .show_console = false,
//    .settings = nullptr,
    .config_file = "",
    .post_data_file = "",
    .command_line_args = {},
    .dragndrop_file = false
};

config_t config {
    .timeout = 10,
    .max_log_file_size = 50000, // bytes

    .http_to_stdout = false,
    .header = QHash<QString, QString>(),
    .user_select = false,

    .url = "",
    .file_form_name = "",
    .file_min_size = 0,
    .file_max_size = 0,
    .post_data_name = "",
    .post_data = "",
    .post_data_content_type = "",

    .result_var = QHash<QString, QString>(),
    .command =  "",

    // branding
    .application_title = "",
    .application_header = "",
    .application_copyright = "",
    .application_text = "",
    .application_text_error = "",
    .application_image = "",

    .pre_command = "",
    .default_mode = "",
    .pre_command_exit_codes = QList<int>({})
};

QVector<QString> availableModes({});
