#include "uploaderlib.h"

const QString UploaderLib::m_allowedVarChar = "[a-zA-Z0-9_-]";

UploaderLib::UploaderLib(QObject *parent) :
    QObject(parent)
{

}

QString UploaderLib::preparePayload(QString postTemplate, QHash<QString, QString> variables) {
    QString payload(postTemplate);

    // if there is a post-data in the config, replace variables
    if (payload != "") {

        // replace payload variables
        QHash<QString, QString>::const_iterator i = variables.constBegin();
        while (i != variables.constEnd()) {
            //qDebug() << "replacing " << i.key() << "with" << i.value();
            payload.replace("$" + i.key(), i.value());
            ++i;
        }

        // replace environment variables
        payload = expandEnvironmentVariables(payload);
    }

    return payload;
}

// FIXME: add to unit testing
QHash<QString, QString> UploaderLib::getConfigVarList(IniParser *ini, QString prefix, QString mode) {
    QHash<QString, QString> ret = QHash<QString, QString>();
    std::list<string> keys = ini->keys("uploader");

    // parse request headers from config file
    // [uploader] section first
    //for (int i=0; i<keys.size(); i++) {
    for (std::list<string>::iterator it = keys.begin(); it != keys.end(); it++) {
        QString k = QString::fromStdString(*it);
        //qDebug() << "uploader" << "/" << k;
        if (k.indexOf(prefix + "-") == 0) {
            int dash = k.indexOf("-");
            QString ks(k.mid(dash+1, k.length()));
            //qInfo() << "uploader/" << ks << ": " << QString::fromStdString(ini->get("uploader", k.toStdString()));
            ret.insert(ks, QString::fromStdString(ini->get("uploader", k.toStdString())));
            break;
        }
    }

    QString modestr(QString("mode-" + mode));
    keys = ini->keys(modestr.toStdString());
    for (std::list<string>::iterator it = keys.begin(); it != keys.end(); it++) {
        QString k = QString::fromStdString(*it);
        //qDebug() << mode << "/" << k;
        if (k.indexOf(prefix + "-") == 0) {
            int dash = k.indexOf("-");
            QString ks(k.mid(dash+1, k.length()));
            //qInfo() << mode << "/" << ks << ": " << QString::fromStdString(ini->get(mode.toStdString(), k.toStdString()));
            ret.insert(ks, QString::fromStdString(ini->get(modestr.toStdString(), k.toStdString())));
            break;
        }
    }

    return ret;
}

QVariant UploaderLib::getConfigVar(IniParser *ini, QString name, QString mode, QVariant defaultval) {
    std::string kg = "uploader";
    std::string kd = "mode-";
    kd.append(mode.toStdString());

    if (ini->exists(kd, name.toStdString())) {
        QString v = QString::fromStdString(ini->get(kd, name.toStdString()));
        return QVariant(v);
    } else if (ini->exists(kg, name.toStdString())) {
        QString v = QString::fromStdString(ini->get(kg, name.toStdString()));
        return QVariant(v);
    }

    return defaultval;
}

QStringList UploaderLib::parseOptions(QStringList arguments) {
    QStringList options(arguments);
    QStringList ret({});
    for (int i=0; i<options.length(); i++) {
        QString o = options.at(i);
        if (o.indexOf("--") == 0) {
            QStringList pair(o.mid(2, o.length()).split("="));

            if (pair.length() == 2) {
                //qDebug() << pair;
                programArguments.parameter.insert(pair.at(0), pair.at(1));
            }
        } else {
            ret.append(o);
        }
    }

    return ret;
}

UploaderLib::UploaderLibError UploaderLib::applicationInit(QString executableDirectory) {
    // FIXME: not sure if this particularly clever or plain stupid !?
    //        we are changing a global that has been set during
    //        program initialisation.
    programArguments.exe_dir.setFileName(executableDirectory);

    // check if config file has been set by command line argument `-s`
    // if not, use default "uploader.ini" in program folder
    qDebug() << "programArguments.config_file: " << programArguments.config_file;
    if (programArguments.config_file == "") {
        programArguments.config_file = executableDirectory;

        // check if the last character is as "/"
        if (programArguments.config_file.right(1) != "/")
            programArguments.config_file.append("/");

        programArguments.config_file.append("uploader.ini");
        //qInfo() << "config_file: " << programArguments.config_file;
    }

    // check if config file exists
    if (!QFile(programArguments.config_file).exists())
        return CONFIG_NOT_READABLE;

    return OK;
}

UploaderLib::UploaderLibError UploaderLib::parseCommandLineArguments(QStringList arguments) {
    QCommandLineParser parser;
    parser.addPositionalArgument("file", "Path to upload file");

    // mode `-m=mode-name`
    QCommandLineOption modeOption(QStringList() << "m",
        "Operation: upload|with-param|select", "mode");
    parser.addOption(modeOption);

    // show console (-c)
    QCommandLineOption showConsole(QStringList() << "c",
            "Show console.");
    parser.addOption(showConsole);

    // show console (-h)
    QCommandLineOption showHelp(QStringList() << "h",
            "This help.");
    parser.addOption(showHelp);

    // settings file (-s)
    QCommandLineOption uploaderIni(QStringList() << "s",
            "path to uploader.ini.", "iniFile", NULL);
    parser.addOption(uploaderIni);

    // Process the actual command line arguments given by the user
    QStringList options(parseOptions(arguments));
    qDebug() << "options: " << options;
    parser.parse(options);

    // show help
    if (parser.isSet(showHelp)) {
        parser.showHelp(0);
        return UploaderLib::SHOW_HELP;
    }

    programArguments.command_line_args = parser.positionalArguments();
    programArguments.show_console = parser.isSet(showConsole);

    // if we have a dnd target, add file name to the list of arguments
    if (programArguments.dragndrop_file) {
        programArguments.command_line_args << programArguments.file.fileName();
    }

    // input validation
    // check if we have a command line argument mode. if drag&drop is active, check after
    // the ini file is parsed, only then do we know if a default-mode is configured
    QString mode = parser.value(modeOption).toLower();
    if (mode == "" && !programArguments.dragndrop_file)
        // return handle_error(a, w, "Invalid mode, `-m="+parser.value(modeOption)+"`.");
        return UploaderLib::NO_MODE;

    //printf("mode %s\n", mode.toStdString().c_str());
    programArguments.mode = mode;

    // check if we have a path to a setting file
    QString iniFile = parser.value(uploaderIni);
    if (iniFile != NULL) {
        programArguments.config_file = iniFile;
    }

    return UploaderLib::OK;
}

UploaderLib::UploaderLibError UploaderLib::readSettings() {
    // prevent reading into directories
    QFileInfo info(programArguments.config_file);
    if (!info.isFile())
        return SETTINGS_NOT_EXIST;

    // check if config file exists
    if (!QFile(programArguments.config_file).exists())
        return SETTINGS_NOT_EXIST;

    // read settings file
    IniParser *ini = new IniParser();
    int ret = ini->open(programArguments.config_file.toStdString());
    if (ret != 0) {
        delete ini;
        return SETTINGS_PARSE_ERROR;
    }

    if (programArguments.mode == "") {
        config.default_mode = UploaderLib::getConfigVar(ini, "default-mode", "", "").toString().trimmed();
        qDebug() << "programArguments.mode: " << programArguments.mode << ", config.default_mode: " << config.default_mode;

        if (config.default_mode != "")
            programArguments.mode = config.default_mode;
        else
            return SETTINGS_NO_MODE;
    }

    // create list of available modes
    list<string> sections = ini->sections();
    list<string>::iterator it;
    //qInfo() << "mode: " << programArguments.mode;
    for (auto const& sec : sections) {
        //qInfo() << QString::fromStdString(sec);
        string::size_type loc = sec.find(QString("-").toStdString(), 0);
        //qInfo() << loc;
        if( loc != string::npos ) {
            // strip "mode-" from name
            std::string sec_name = sec.substr(5, sec.length());
            QString n(sec_name.c_str());
            availableModes.append(n);
        }
    }

    // check if current mdoe exists
    //qInfo() << availableModes;
    if (availableModes.indexOf(programArguments.mode) == -1) {
        return SETTINGS_INVALID_MODE;
    }

    // read configs and set defaults for empty values
    config.header = getConfigVarList(ini, "header", programArguments.mode);
    config.result_var = getConfigVarList(ini, "resultvar", programArguments.mode);

    config.timeout        = UploaderLib::getConfigVar(ini, "timeout", programArguments.mode, 3).toInt();
    config.max_log_file_size = UploaderLib::getConfigVar(ini, "max-log-file-size", "", 500000).toInt(); // this has to be set globally

    config.http_to_stdout = UploaderLib::getConfigVar(ini, "http-to-stdout", programArguments.mode, false).toBool();

    config.url            = UploaderLib::getConfigVar(ini, "url", programArguments.mode, "").toString();
    config.file_min_size  = UploaderLib::getConfigVar(ini, "file-min-size", programArguments.mode, 0).toInt();
    config.file_max_size  = UploaderLib::getConfigVar(ini, "file-max-size", programArguments.mode, 0).toInt();
    config.file_form_name = UploaderLib::getConfigVar(ini, "file-form-name", programArguments.mode, "").toString();
    config.post_data_name = UploaderLib::getConfigVar(ini, "post-data-name", programArguments.mode, "").toString();
    config.post_data_content_type = UploaderLib::getConfigVar(ini, "post-data-content-type", programArguments.mode, "").toString().trimmed();
    config.user_select    = UploaderLib::getConfigVar(ini, "user-select", programArguments.mode, false).toBool();
    config.command        = UploaderLib::getConfigVar(ini, "command", programArguments.mode, "").toString().trimmed();
    config.pre_command    = UploaderLib::getConfigVar(ini, "pre-command", programArguments.mode, "").toString().trimmed();

    QString pre_command_exit_codes  = UploaderLib::getConfigVar(ini, "pre-command-exit-codes", programArguments.mode, "0").toString().trimmed();
    // split and add it to the list of allowed exit codes
    if (pre_command_exit_codes != "0" && pre_command_exit_codes != "") {
        QRegExp rx("[, \t]+");
        QStringList codes = pre_command_exit_codes.split(rx);
        config.pre_command_exit_codes.clear();
        for (auto const& c : codes) {
            config.pre_command_exit_codes.append(c.toInt());
        }
    }

    config.application_title  = UploaderLib::getConfigVar(ini, "application-title", programArguments.mode, "Uploader").toString().trimmed();
    config.application_header = UploaderLib::getConfigVar(ini, "application-header", programArguments.mode, "HTTP Uploader").toString().trimmed();
    config.application_copyright = UploaderLib::getConfigVar(ini, "application-copyright", programArguments.mode, "").toString().trimmed();
    config.application_text = UploaderLib::getConfigVar(ini, "application-text", programArguments.mode, "").toString().trimmed();
    config.application_text_error = UploaderLib::getConfigVar(ini, "application-text-error", programArguments.mode, "").toString().trimmed();
    config.application_image = UploaderLib::getConfigVar(ini, "application-image", programArguments.mode, "").toString().trimmed();

    programArguments.post_data_file = UploaderLib::getConfigVar(ini, "post-data", programArguments.mode, "").toString().trimmed();

    // free ini memory
    delete ini;

    // expand environment variables in pre-command
    config.pre_command = UploaderLib::expandEnvironmentVariables(config.pre_command);

    // post data template is stored in a file, find it, read it or throw exception
    if (programArguments.post_data_file != "") {

        QFileInfo info(programArguments.post_data_file);
        if (info.isRelative()) {
            // post_data_file = programArguments.exe_dir.fileName() + "/" + post_data_file;
            int pos = programArguments.config_file.lastIndexOf(QChar('/'));
            programArguments.post_data_file = programArguments.config_file.left(pos)  + "/" + programArguments.post_data_file;
            // qDebug() << "post_data_file: " << post_data_file;
        }

        // prevent reading directories
        //qInfo() << "post_data_file: " << post_data_file;
        info = programArguments.post_data_file;
        if (!info.isFile())
            return SETTINGS_PAYLOAD_NOT_EXIST;

        // check if it exists and is readable
        QFile f(programArguments.post_data_file);
        if (!f.exists())
            return SETTINGS_PAYLOAD_NOT_EXIST;

        if (!f.open(QIODevice::ReadOnly)) {
            f.close();
            return SETTINGS_PAYLOAD_NOT_READABLE;
        }

        // read content
        config.post_data = f.readAll();
        f.close();
    }

    //qInfo() << post_data << " " << post_data.typeName();
    if (config.post_data_content_type == "")
        config.post_data_content_type = "text/plain";

    // check if the minimal configuration is met
    if (config.url == "" || config.file_form_name == "") {
        return SETTINGS_INVALID_CONFIGURATION;
    }

    return OK;
}

/**
 * @brief UploaderLib::fileIsReadable
 * @return 0 on success, 1 if no file name is supplied, 2 if file does not exist, 3 if file is not readable
 */
UploaderLib::UploaderLibError UploaderLib::fileIsReadable() {
    if (!config.user_select) {
        if (programArguments.command_line_args.length() != 1)
            return FILE_PARAM_MISSING;

        QString file = expandEnvironmentVariables(programArguments.command_line_args.at(0));
        programArguments.file.setFileName(file);
        if (!programArguments.file.exists())
            return FILE_NOT_EXISTS;

        if (!programArguments.file.open(QIODevice::ReadOnly)) {
            programArguments.file.close();
            return FILE_NOT_READABLE;
        }

        programArguments.file.close();
    }

    return OK;
}

QString UploaderLib::expandEnvironmentVariables(QString path) {
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    QStringList vars = env.keys();
    QString out = QString(path);

//#if defined(Q_OS_WIN)
//    QRegExp rx("%([a-zA-Z0-9_]+)%");
//#else
    QRegExp rx("\\$(\\{?[a-zA-Z0-9_]+\\}?)");
    //qInfo() << "vars: " << vars;
//#endif
    int pos = 0;

    while ((pos = rx.indexIn(path, pos)) != -1) {
        QString match = rx.cap(1);
        pos += rx.matchedLength();

        bool quoted = false;
        QString varname(match);
        if (varname.at(0) == '{') {
            varname = varname.mid(1, varname.length()-2);
            quoted = true;
        }

        //qDebug() << "varname: " << varname;

        // check if we know this variable
        //QStringList vars_match = vars.filter(varname);
        //qDebug() << "vars_match: " << vars_match;
        if (vars.contains(varname)) {
//#if defined(Q_OS_WIN)
//            out.replace("%" + match + "%", env.value(match));
//#else
            //out.replace("$" + match, env.value(match));
            if (!quoted) {
                const QRegExp re1{"(\\$"+QRegExp::escape(match)+")([^[a-zA-Z0-9_])"};
                out.replace(re1, env.value(varname) + "\\2");

                const QRegExp re2{"(\\$"+QRegExp::escape(match)+")$"};
                out.replace(re2, env.value(varname));
            } else {
                const QRegExp re3{"(\\$"+QRegExp::escape(match)+")"};
                out.replace(re3, env.value(varname));
            }
//#endif
        }
    }

    return out;
}

httpResponse_t UploaderLib::parseHtttpResponse(QString response) {
    httpResponse_t ret = {
      .statusCode = 0,
      .contentType = QString("text/plain"), // assumed as default
      .contentLength = 0,
      .body = QString(""),
      .header = QHash<QString, QString>(),
      .error = 0,
      .errorMessage = QString("")
    };

    // check for empty string, if so don't even bother
    if (response.trimmed() == "") {
        ret.error = 1;
        ret.errorMessage = "Empty http Response.";
        return ret;
    }

    // split string by \r\n\r\n (seperate header from body)
    int posOfSeparator = response.indexOf("\r\n\r\n");
    if (posOfSeparator == -1) {
        ret.error = 2;
        ret.errorMessage = "Failed to find header and body separator";
        return ret;
    }

    // check if there are prelimenary headers, skip them.
    if (response.mid(posOfSeparator+4, 4) == "HTTP") {
        if (response.length() < posOfSeparator+4) {
            ret.error = 1;
            ret.errorMessage = "Empty http Response";
            return ret;
        }
        response = response.mid(posOfSeparator+4);
        posOfSeparator = response.indexOf("\r\n\r\n");
        if (posOfSeparator == -1) {
            ret.error = 2;
            ret.errorMessage = "Failed to find header and body separator";
            return ret;
        }
    }

    // parse headers
    QString headers = response.left(posOfSeparator);
    QStringList headerLines = headers.split("\r\n");

    //qDebug() << "Response Header Lines: " << header;
    for (const auto& i : headerLines) {

        // parse response status code
        if (i.left(4) == "HTTP") {
            QStringList parts = i.split(" ");
            //qDebug() << parts.length();
            if (parts.length() < 2) {
                ret.error = 3;
                ret.errorMessage = "Malformed HTTP Status line";
                return ret;
            }

            // check if status code consists of digits only
            QRegExp re("^\\d*$");  // a digit (\d), zero or more times (*)
            if (!re.exactMatch(parts.at(1))) {
                ret.error = 3;
                ret.errorMessage = "Malformed HTTP Status line, status code not numeric.";
                return ret;
            }

            // parse response code
            ret.statusCode = parts.at(1).toInt();
            continue;
        }

        // store header in our hash
        QStringList kv = i.trimmed().split(": ");
        if (kv.size() == 2)
            ret.header.insert(kv.at(0).toLower(), kv.at(1));
        else if (kv.size() == 1)
            ret.header.insert(kv.at(0).toLower(), "");

        // remeber the content-type
        if (kv.size() == 2 && kv.at(0).toLower() == "content-type") {
            ret.contentType = kv.at(1).toLower();
        }
    }

    // copy the rest of the data into the body variable
    int responseLength = response.length();
    if (responseLength > posOfSeparator+4)
        ret.body = response.mid(posOfSeparator+4);
    // else, empty body, header only response.

    // remeber length
    ret.contentLength = responseLength - posOfSeparator - 4;

    return ret;
}

jsonResult_t UploaderLib::extractFromJson(QString jsonContent, QString jsonPointer) {
    jsonResult_t ret = {
        .value = QString(""),
        .error = 0,
        .errorMessage = QString("")
    };

    json json_parsed;

    // try to parse the json content
    try {
        json_parsed = json::parse(jsonContent.toStdString());
    } catch (json::parse_error& e) {
        ret.error = e.id;
        ret.errorMessage = e.what();
        ret.errorMessage.append(", pos: " + QString::number(e.byte));

        return ret;
    }

    // try to pare the json pointer
    // https://nlohmann.github.io/json/api/json_pointer/
    json::json_pointer p;
    try {
        p = json::json_pointer(jsonPointer.toStdString());
    } catch (json::exception& e) {
        ret.error = e.id;
        ret.errorMessage = e.what();

        return ret;
    }

    // this is untrustworthy user input
    json val;
    try {
        val = json_parsed[p];
    } catch (json::exception& e) {
        ret.error = e.id;
        ret.errorMessage = e.what();

        return ret;
    }

    // https://nlohmann.github.io/json/doxygen/classnlohmann_1_1basic__json_a5b7c4b35a0ad9f97474912a08965d7ad.html
    // https://nlohmann.github.io/json/features/types/
    auto t = val.type_name();
    if (val.type() == json::value_t::null) { // element not found or 'null'
        ret.error = -3;
        ret.errorMessage = "invalid type null, cannot convert to scalar.";
        return ret;
    } else if (val.type() == json::value_t::number_integer ||
               val.type() == json::value_t::number_unsigned ) {
        ret.value = QString::number(val.get<std::double_t>(), 'f', 0);
    } else if (val.type() == json::value_t::number_float)
        ret.value = QString::number(val.get<std::double_t>(), 'f');
    else if (strcmp("null", t) == 0)
        ret.value = QString("");
    else if (strcmp("boolean", t) == 0)
        ret.value = (val) ? QString("true") : QString("false");
    else if (strcmp("string", t) == 0)
        ret.value = QString::fromStdString(val);
    else if (strcmp("array", t) == 0) {
        ret.error = -1;
        ret.errorMessage = "invalid type array, cannot convert to scalar.";
        return ret;
    } else if (strcmp("object", t) == 0) {
        ret.error = -2;
        ret.errorMessage = "invalid type object, cannot convert to scalar.";
        return ret;
    } else {
        ret.error = -4;
        ret.errorMessage = "invalid unknown type, cannot convert to scalar.";
        return ret;
    }

    return ret;
}

jsonResultVar_t UploaderLib::findResultVars(QHash<QString, QString> result_var, QString httpBody) {
    jsonResultVar_t res = {
        .variables = QHash<QString, QString>(),
        .error = 0,
        .errorMessage = ""
    };

    QHashIterator<QString, QString> i (result_var);
    while (i.hasNext()) {
        i.next();
        jsonResult_t val = UploaderLib::extractFromJson(httpBody, i.value());
        if (val.error != 0) {
            res.error = val.error;
            res.errorMessage = "Error: failed to parse response json, reason: " + val.errorMessage;
            return res;
        }
        res.variables.insert(i.key(), val.value);
    }

    return res;
}

varSubtitution_t UploaderLib::substituteVariables(QHash<QString, QString> variables, QString text) {
    varSubtitution_t res = {
        .value = QString(""),
        .error = 0,
        .errorMessage = QString("")
    };

    // get the command, substitute all variables and we are done
    QString out = text;
    QHashIterator<QString, QString> it (variables);
    while (it.hasNext()) {
        it.next();
        out.replace("$"+it.key(), it.value());
    }

    // check if we still have some variable literals in the string
    QRegularExpression re("\\$" + UploaderLib::m_allowedVarChar + "+");
    QRegularExpressionMatch match = re.match(out);
    if (match.hasMatch()) {
        QString varnames("");
        for (int i = 0; i <= match.lastCapturedIndex(); ++i) {
            if (i != 0) varnames.append(", ");
            varnames.append(match.captured(i));
        }

        res.error = 1;
        res.errorMessage = "Some variables could not be replaced: " + varnames;
        return res;
    }

    res.value = out;
    return res;
}


QStringList UploaderLib::splitCommandLine(QString commandline) {

    QStringList arguments{};
    bool insideQuote = false;
    QString buffer{""};

    for (char c: commandline.toUtf8()) {
        if (c == '"') {
            insideQuote = !insideQuote;
            continue;
        }

        if (c == ' ' && !insideQuote) {//when space is not inside quote split..
            arguments << buffer; //token is ready, lets add it to list
            buffer = "";
        } else
            buffer.append(c);//else add character to token
    }

    // lets not forget about last token that doesn't have space after it
    arguments << buffer;

    if (arguments.size() == 0 || insideQuote)
        return QStringList();

    return arguments;
}

/**
 * @brief Run a process
 * @param commandline
 * @return 0 on succes, -1 on itnernal error > 0 when program exist code is != 0
 *
 * NOTE: This method must not be called from within the main loop.
 */
UploaderLib::UploaderLibError UploaderLib::preExecute(QString commandline) {

    if (commandline == "")
        return UploaderLibError::EXECUTE_CMD_EMPTY;

    QStringList arguments = splitCommandLine(commandline);

    if (arguments.size() == 0)
        return UploaderLibError::EXECUTE_QUTE_ERROR;

    QString cmd = arguments.at(0);
    arguments.pop_front();

    //qInfo() << "cmd: " << cmd;
    //qInfo() << "tokens: " << arguments;

    // launch the command blocking and wait for it to exit.
    QProcess proc;
    proc.start(cmd, arguments);
    if (!proc.waitForStarted())
        return UploaderLibError::EXECUTE_PROCESS_FAILED_TO_START;

    //qInfo() << "pid " << proc.processId();

    if (!proc.waitForFinished(-1))
        return UploaderLibError::EXECUTE_PROCESS_FINISHED_UNECPECTED;

    QByteArray result = proc.readAll();
    //qDebug() << "result : " << result << ", exit code: " << proc.exitCode();

    // return the exit code if launching was not successfull
    int exit_code = proc.exitCode();
    //qDebug() << "exit_code: " << QString::number(exit_code) << " " << config.pre_command_exit_codes
    //    << " find: " << config.pre_command_exit_codes.indexOf(exit_code);
    if (exit_code != 0 && config.pre_command_exit_codes.indexOf(exit_code) != -1) {
        /*
        QCoreApplication *app = qApp->instance();
        app->quit();
        */
        qInfo() << "Exiting application, got handled exit code from pre-command: " + QString::number(exit_code);
        exit(0);
    }

    return OK;
}

void UploaderLib::pruneLogFile() {
    // pruning log file
    //
    // only try this if we suceeded in opening the log file in the first place
    if (programArguments.log_file_open) {
        // close the file first
        programArguments.log_file.flush();
        programArguments.log_file.close();
        programArguments.log_file_open = false;

        // get the file size
        qint64 size = programArguments.log_file.size();
        qDebug() << "size: " << size << ", max_log_file_size: " << config.max_log_file_size;

        if (size > config.max_log_file_size) {
            qint64 remove = size - config.max_log_file_size;

            QFile ro_log(programArguments.log_file.fileName());
            ro_log.open(QIODevice::ReadOnly);
            ro_log.seek(remove);

            // forward to the next newline
            ro_log.readLine();

            // now read the reminder into a string buffer
            QByteArray log_buffer = ro_log.readAll();
            ro_log.close();

            // open for writing again, truncate
            programArguments.log_file.open(QIODevice::WriteOnly);
            programArguments.log_file.write(log_buffer);
            programArguments.log_file.close();
        }

    }
}

UploaderLib::UploaderLibError UploaderLib::parseUrl(QString string_url) {
    // parse url and query parameters
    QUrl url(string_url, QUrl::TolerantMode); // will allow '%' and other small mistakes

    // check if we had a parsing error
    if (!url.isValid()) {
        url.clear();
        return URLPARSE_MALFORMED_URL;
    }

    QUrlQuery query(url.query());
    QList<QPair<QString, QString>> queryPairs = query.queryItems();

    qDebug() << "host: "  << url.host();
    qDebug() << "path: "  << url.path();
    qDebug() << "query: "  << url.query();

    // we have a parsed url, do a sanity check, we atl least expect
    // to have a non empty mode parameter
    if (url.host() == "") {
        //return handle_error(a, 2, "Mode is not defined");
        return UploaderLib::URLPARSE_NO_MODE;
    }

    // check if we have a file
    if (url.path() == "") {
        // return handle_error(a, 3, "File is not defined");
        return UploaderLib::URLPARSE_NO_FILE;
    }

    // generate list of commandline arguments
    //QStringList arguments{"-m=" + url.host()};
    programArguments.mode = url.host();

    for (auto const& kv: queryPairs) {
        qDebug() << " - " << kv.first << ": " << kv.second;

        if (kv.first.left(2) == "_c") {
            QStringList consolekv = kv.first.split("=");
            bool enabled = true;

            // check value
            if (consolekv.length() == 2) {
                QString v = consolekv.at(1).toLower();
                if (v == "0" || v == "false")
                    enabled = false;
            }

            if (enabled)
                programArguments.show_console = true;

            continue;
        }

        //QString arg = "--" + kv.first + "=\"" + kv.second + "\"";
        //programArguments.command_line_args << arg;
        programArguments.parameter.insert(kv.first, kv.second);
    }

    // add file to the arguments, as last parameter
    // FIXME: remove first slash when absolute ?
    QString file = url.path();
    if (url.path().at(2) == ':') // windows drive letter, remove staring slash
        file = file.mid(1);

    file = UploaderLib::expandEnvironmentVariables(file);
    programArguments.command_line_args << file;
    programArguments.file.setFileName(file);

    /*

    // when the sanity check is finished, run the external command
    // <exec dir>/uploader.exe
    QString path = QCoreApplication::applicationDirPath();

    // in QT_DEBUG we will run <exec dir>/../uploader/uploader.exe
    // due to a different directory structure to the deployed app
#ifdef QT_DEBUG
    path += "/../uploader/";
#endif

    // windows, will need '.exe' in the file name
#if defined(Q_OS_WIN)
    QString exe(path + "/uploader.exe");
#else
    QString exe(path + "uploader");
#endif

    // run uploader.exe in a separate (detatched) process, make sure
    // we are not the parent, so we can exit even if exe keeps running.
    qDebug() << "exe: " << exe;
    qDebug() << "arguments: " << arguments;

    bool ret = QProcess::startDetached(exe, arguments, path);
    if (!ret) {
        // return handle_error(a, 4, "Failed to execute `uploader.exe`");
        return URLPARSE_NO_UPLOADER;
    }
    */

    return OK;
}
