# uploaderTest - unit test project

## prerequisites

- place `upload.php` on a test server
- make sure the test server's upload limits are configured properly
    - `upload_max_filesize` = 128M
    - `post_max_size` = 128M
- change all `url` instances in `uploader.ini` to point to that server.
- command line arguments are set in `main()`, the one you are passing into the executable are used to control `qTest`, not the argument parser

## Build

    cd ../build
    rm -rf *
    qmake ../uploader.pro
    make
    
# Run

    cd ../build/uploadTest
    ./uploadTest

Output should look something like:
    
    ...
    Totals: 8 passed, 0 failed, 0 skipped, 0 blacklisted, 136ms
    ********* Finished testing of uploaderTest *********
