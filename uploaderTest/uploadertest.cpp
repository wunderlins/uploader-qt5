#include "uploadertest.h"

uploaderTest::uploaderTest(QObject *parent) : QObject(parent),
    m_thread(this),
    m_responseHeaderAndBody(""),
    m_responseBody(""),
    m_responseHeader()
{
    //connect(&m_thread, &UploadThread::httpResponse, this, &uploaderTest::httpResponse);
}

void uploaderTest::initTestCase() {
    qInfo() << "Program name: " << STRINGIFY(NAME);
    qInfo() << "Program version: " << STRINGIFY(VERSION);
    qInfo() << "Git version: " << STRINGIFY(GIT_VERSION);
}

void uploaderTest::cleanupTestCase() {
    qInfo() << "Cleanup";
}

void uploaderTest::httpResponse(QString responseHeaderAndBody) {
    qDebug() << "== HTTP RESPONSE ==================================";
    qDebug() << responseHeaderAndBody;
    //log(responseHeaderAndBody);
}

void uploaderTest::expandEnvironmentVariablesLibTest() {
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();

    qputenv("MY_VAR", "123");

#if defined(Q_OS_WIN)
    QString search = "$HOMEPATH/.whatever";
    QString match = env.value("HOMEPATH") + "/.whatever";
#else
    QString search = "$HOME/.whatever";
    QString match = env.value("HOME") + "/.whatever";
#endif
    QString path = UploaderLib::expandEnvironmentVariables(search);

    //qDebug() << "path : " << path << ", match: " << match;
    QVERIFY(path == match);

    // test special cases
    // middle of string
    QString middle = UploaderLib::expandEnvironmentVariables("start $MY_VAR end");
    QVERIFY(middle == "start 123 end");

    // end of string
    QString end = UploaderLib::expandEnvironmentVariables("end $MY_VAR");
    //qDebug() << "end: '" << end << "'";
    QVERIFY(end == "end 123");

    // end of string quoted
    QString endq = UploaderLib::expandEnvironmentVariables("endq ${MY_VAR}");
    //qDebug() << "endq: '" << endq << "'";
    QVERIFY(endq == "endq 123");

    // inbetween
    QString inbetw = UploaderLib::expandEnvironmentVariables("inb${MY_VAR}etw");
    qDebug() << "inbetw: '" << inbetw << "'";
    QVERIFY(inbetw == "inb123etw");
}

/**
 * Test Application initialisation
 *
 * Mainly find config file.
 *
 * @brief uploaderTest::uploaderApplicationInitTest
 */
void uploaderTest::uploaderApplicationInitTest() {
    if (disableAppInitialisation)
        QSKIP("AppInitialisation disabled");

    // try to load an unexistent config file
    QString app_dir = QFINDTESTDATA("whatever");
    //qInfo() << "part of the test is to use an invalid location, ignore above WARNING";
    //qInfo() << "app_dir " << app_dir;
    int ret = UploaderLib::applicationInit(app_dir + "../");
    QVERIFY2(ret == UploaderLib::CONFIG_NOT_READABLE, "config file should not be found, we are pointing to a directory.");

    // reset config file location
    programArguments.config_file = "";

    // try to load the real config file
    app_dir = QFINDTESTDATA("");
    qInfo() << "app_dir " << app_dir;
    //ret = UploaderLib::applicationInit(app_dir + "../build/uploaderTest/");
    ret = UploaderLib::applicationInit(QCoreApplication::applicationDirPath());
    QVERIFY2(ret == UploaderLib::OK, "config file should be in the build dir.");

    if (ret != 0) {
        qDebug() << "Failed to read config directory";
        exit(127);
    }

    // global programArguments contains now config_file and exe dir
    QVERIFY2(programArguments.exe_dir.exists(), "Application dir should exist.");
    QVERIFY2(programArguments.config_file != "", "config_file should not be empty.");
}

void uploaderTest::parseUrlLibTest() {
    // test the command line argument as url
    QString string_url = "ul://mode/file?a=1&b=2";

    // test valid url
    UploaderLib::UploaderLibError ret;
    ret = UploaderLib::parseUrl(string_url);

    QVERIFY2(ret == UploaderLib::OK, QString("Expected [0], got [%1]").arg(ret).toStdString().c_str());
    QVERIFY2(programArguments.mode == "mode", QString("Expected [mode], got [%1]").arg(programArguments.mode).toStdString().c_str());
    QVERIFY2(programArguments.parameter.size() == 2, QString("Expected [2], got [%1]").arg(programArguments.parameter.size()).toStdString().c_str());
    QVERIFY2(programArguments.parameter.value("a") == "1", QString("Expected [1], got [%1]").arg(programArguments.parameter.value("a")).toStdString().c_str());
    QVERIFY2(programArguments.parameter.value("b") == "2", QString("Expected [1], got [%1]").arg(programArguments.parameter.value("b")).toStdString().c_str());
    QVERIFY2(programArguments.file.fileName() == "/file", QString("Expected [file], got [%1]").arg(programArguments.file.fileName()).toStdString().c_str());

    // test malformed url
    string_url = "ul\\::::";
    ret = UploaderLib::parseUrl(string_url);
    QVERIFY2(ret == UploaderLib::URLPARSE_MALFORMED_URL, QString("Expected [%1], got [%2]").arg(UploaderLib::URLPARSE_MALFORMED_URL).arg(ret).toStdString().c_str());

    // test no mode
    string_url = "ul://";
    ret = UploaderLib::parseUrl(string_url);
    QVERIFY2(ret == UploaderLib::URLPARSE_NO_MODE, QString("Expected [%1], got [%2]").arg(UploaderLib::URLPARSE_NO_MODE).arg(ret).toStdString().c_str());

    // test no file
    string_url = "ul://host";
    ret = UploaderLib::parseUrl(string_url);
    QVERIFY2(ret == UploaderLib::URLPARSE_NO_FILE, QString("Expected [%1], got [%2]").arg(UploaderLib::URLPARSE_NO_FILE).arg(ret).toStdString().c_str());

    // reset global variables
    programArguments.mode = "";
    programArguments.command_line_args.clear();
    programArguments.parameter.clear();
}

/**
 * Test command line parameter parsing
 *
 * @brief uploaderTest::parseCommandLineArgumentsTest
 */
void uploaderTest::parseCommandLineArgumentsTest() {
    if (disableAppInitialisation)
        QSKIP("AppInitialisation disabled");

    QStringList args = QCoreApplication::arguments();
    // qInfo() << args;
    UploaderLib::UploaderLibError status = UploaderLib::OK;

    // test with help switch
    /*
    QStringList argHelp({"-h"});
    status = UploaderLib::parseCommandLineArguments(argHelp);
    QVERIFY2(status == UploaderLib::SHOW_HELP, "should return SHOW_HELP");
    */

    // test without mode
    QStringList argNoMode({"-c", args[4]});
    status = UploaderLib::parseCommandLineArguments(argNoMode);
    QVERIFY2(status == UploaderLib::NO_MODE, "-m omitted should raise an error");

    QStringList argEmptyMode({"-c", "-m=", args[4]});
    status = UploaderLib::parseCommandLineArguments(argEmptyMode);
    QVERIFY2(status == UploaderLib::NO_MODE, "-m= should rasie an error");

    QStringList argNoparamMode({"-c", "-m"});
    status = UploaderLib::parseCommandLineArguments(argNoparamMode);
    QVERIFY2(status == UploaderLib::NO_MODE, "`-m` without parameter should rasie an error");

    // test with drop target
    QStringList argNofileMode({"-c"});
    programArguments.file.setFileName(args[4]);
    programArguments.dragndrop_file = true;
    status = UploaderLib::parseCommandLineArguments(argNofileMode);
    QVERIFY2(status == UploaderLib::OK, "failed to Test with drop target");

    // test '-s file.ini' option
    QString cfgFile("-s=");
    cfgFile.append(programArguments.exe_dir.fileName());
    cfgFile.append("/../../resources/uploaderTest.ini");
    QStringList argConfigMode({"-c", "-m=with-param", cfgFile, args[4]});
    programArguments.file.setFileName(args[4]);
    programArguments.dragndrop_file = false;
    qDebug() << "argConfigMode: " << argConfigMode;
    status = UploaderLib::parseCommandLineArguments(argConfigMode);
    qDebug() << "argConfigMode status: " << status;
    QVERIFY2(status == UploaderLib::OK, "failed to Test with config file");
    QVERIFY2(programArguments.config_file.compare(cfgFile), "failed to Test with config file");

    // final test for our test data
    programArguments.file.setFileName("");
    programArguments.dragndrop_file = false;
    QStringList argOkMode(args);
    status = UploaderLib::parseCommandLineArguments(argOkMode);
    QVERIFY2(status == UploaderLib::OK, "should get valid parameters with pid/fid");
    QVERIFY(programArguments.parameter.find("fid").value() == "1234");
    QVERIFY(programArguments.parameter.find("pid").value() == "5678");
}

void uploaderTest::iniLibTest() {
    if (disableIniParser)
        QSKIP("Ini Parser Disabled");

    QString ini_file = QFINDTESTDATA("") + "../build/uploaderTest/uploader.ini";
    //qInfo() << "ini_file: " << ini_file;
    QVERIFY2(QFileInfo::exists(ini_file) == true, "Ini File not found");

    IniParser *ini = new IniParser();
    QVERIFY(ini->inifile == nullptr);
    QVERIFY(ini->errnum == 0);
    QVERIFY(ini->fp== nullptr);

    // file not open, section list empty
    std::list<std::string>  sec = ini->sections();
    QVERIFY(sec.size() == 0);

    // parse file
    int ret = ini->open(ini_file.toStdString().c_str());
    QVERIFY(ret == 0);

    // find key
    QVERIFY(ini->exists("uploader", "timeout") == true);
    QVERIFY(ini->get("uploader", "timeout") == "3");

    // find a key by uploaderLib API
    QVariant timeoutv = UploaderLib::getConfigVar(ini, "timeout", "uploader");
    //qInfo() << "timeoutv: " << timeoutv.toInt();

    // get key that does not exist
    QVariant timeouterr = UploaderLib::getConfigVar(ini, "timeout----", "uploader");
    QVERIFY(timeouterr == QVariant());
    bool ok = false;
    QVERIFY(timeouterr.toInt(&ok) == 0);
    QVERIFY(ok == false);

    // get key that does not exist with default value
    QVariant timeouterr2 = UploaderLib::getConfigVar(ini, "timeout----", "uploader", 22);
    QVERIFY(timeouterr2.toInt() == 22);

    /*
    // dump ini file
    std::list<std::string> sections = ini->sections();
    list<string>::iterator it;
    for (auto const& i : sections) {
        qInfo() << "[" << QString::fromStdString(i) << "]";

        list<string> keys = ini->keys(i);
        list<string>::iterator iit;
        for (auto const& key : keys) {
            qInfo() << QString::fromStdString(key) << " = " <<  QString::fromStdString(ini->get(i, key));
        }
    }
    */
    delete ini;
}


/**
 * Test config file parser
 *
 * @brief uploaderTest::readSettingsTest
 */
void uploaderTest::readSettingsTest(){
    if (disableAppInitialisation)
        QSKIP("AppInitialisation disabled");

    // config file path has been setup in uploaderApplicationInitTest()
    QString oldPath = programArguments.config_file;
    int ret = -1;

    // fail, because mode is "" and we don't have a default mode
    programArguments.mode = "";
    ret = UploaderLib::readSettings();
    QVERIFY2(ret == UploaderLib::SETTINGS_NO_MODE,
             QString("mode is empty, parsing connot succeed, ret: [%1]").arg(ret).toStdString().c_str());


    // test with default mode
    QString ini_file_default = QFINDTESTDATA("") + "../resources/uploaderTestDefaultMode.ini";
    programArguments.config_file = ini_file_default;
    ret = UploaderLib::readSettings();
    QVERIFY2(ret == UploaderLib::OK,
             QString("using default mode, expecting 0, got [%1]").arg(ret).toStdString().c_str());

    // fetch a key to see if we have a valid value
    QVERIFY2(config.file_form_name == "uploader1",
             QString("expecting file_form_name to be 'uploader1', got [%1]").arg(config.file_form_name).toStdString().c_str());

    // check the mode list, it should be of length 1 and have only "upload" as mode available
    QVERIFY2(availableModes.length() == 3 && availableModes.indexOf(config.default_mode) == 0,
             QString("expected 3 availableModes, got [%1], first is: [%2]").arg(availableModes.length()).arg(availableModes.at(0)).toStdString().c_str());

    // test with missing payload file
    programArguments.mode = "payload-unexistent";
    ret = UploaderLib::readSettings();
    QVERIFY2(ret == UploaderLib::SETTINGS_PAYLOAD_NOT_EXIST,
             QString("payload file should not be found, expected [%1] got [%2]").arg(UploaderLib::SETTINGS_PAYLOAD_NOT_EXIST).arg(ret).toStdString().c_str());

    // test for missing requried parameter `url` and `file-form-name`
    programArguments.mode = "minimum-req-not-met";
    ret = UploaderLib::readSettings();
    QVERIFY2(ret == UploaderLib::SETTINGS_INVALID_CONFIGURATION,
             QString("minimum config should not be met, expected [%1] got [%2]").arg(UploaderLib::SETTINGS_INVALID_CONFIGURATION).arg(ret).toStdString().c_str());

    // set appropriate mode, reset ini file
    programArguments.mode = "with-param";
    //programArguments.config_file = ini_file;

    // test with directory as file name
    programArguments.config_file = QFINDTESTDATA("../");
    ret = UploaderLib::readSettings();
    QVERIFY2(ret == UploaderLib::SETTINGS_NOT_EXIST,
             QString("expected return code [%1] got [%2]").arg(UploaderLib::SETTINGS_NOT_EXIST).arg(ret).toStdString().c_str());

    // test with inexistent file
    programArguments.config_file = QFINDTESTDATA("../123412341234.ini");
    ret = UploaderLib::readSettings();
    QVERIFY2(ret == UploaderLib::SETTINGS_NOT_EXIST,
             QString("expected return code [%1] got [%2]").arg(UploaderLib::SETTINGS_NOT_EXIST).arg(ret).toStdString().c_str());
    qInfo() << "Previous WARNING is to be expected, we are testing invalid file paths.";
    // reset file name
    programArguments.config_file = oldPath;

    // success?
    ret = UploaderLib::readSettings();
    QVERIFY2(ret == UploaderLib::OK, QString("expected return code [0] got [%1]").arg(ret).toStdString().c_str());

    // verify all config values
    QVERIFY(config.timeout == 3);
    QVERIFY(config.http_to_stdout == false);
    QVERIFY(config.user_select == false);
    //qInfo() << "header: " << config.header;
    //QVERIFY(config.header.find("Authorization").value() == "Basic: user|pass==");
    QVERIFY(config.header.find("HE_USER").value() == "simonrulez-maybe? .... baby?");

    // check directive config
    //qInfo() << config.post_data;
    //QVERIFY(config.url == "http://192.168.0.176/upload.php");
    QVERIFY(config.file_form_name == "uploader1");
    QVERIFY(config.post_data_name == "attributes");
    QVERIFY(config.post_data == "{\n    \"pid\": \"$pid\",\n    \"fid\": \"$fid\",\n    \"unix_user\": \"$USER\",\n    \"win_user\": \"$USERNAME\"\n}\n");

    QVERIFY(config.post_data_content_type == "application/json");
    //qInfo() << "config.command: " << config.command;
    QVERIFY(config.command == "http://google.com?q=host:+$host");

    // check result variables
    QVERIFY(config.result_var.find("host").value() == "/ARRAY/key/2");
}

void uploaderTest::preparePayloadTest() {
    if (disableAppInitialisation)
        QSKIP("AppInitialisation disabled");

    QString my_post_data(config.post_data);

    // add substitution for system specific user in template before testing
#if defined(Q_OS_WIN)
    QString username = qgetenv("USERNAME");
    my_post_data.replace("$USERNAME", username);
#else
    QString username = qgetenv("USER");
    my_post_data.replace("$USER", username);
#endif
    //qDebug() << "user: " << username;

    // we have a payload in the config and some variables from the command line
    // create a working payload
    QString payload = UploaderLib::preparePayload(my_post_data, programArguments.parameter);
    qDebug() << "payload: " << payload;
    if (payload != "") {
        QString expect(my_post_data);
        expect.replace("$fid", programArguments.parameter.find("fid").value());
        expect.replace("$pid", programArguments.parameter.find("pid").value());
        QVERIFY2(payload == expect, QString("Variable subsitution in post paload "
            "failed. got `%1`, expected `%2`").arg(payload).arg(expect).toStdString().c_str());
    } else {
        QVERIFY2(1 == 0, "Paload empty, check configuration");
    }
}

void uploaderTest::fileIsReadableTest() {
    if (disableAppInitialisation)
        QSKIP("AppInitialisation disabled");

    // set file name
    // this is already done insied fileIsReadable();
    // FIXME: setting programArguments.file should happen while command line parsing or reading d&d drop target
    //programArguments.file.setFileName(programArguments.command_line_args.at(0));

    int ret = UploaderLib::fileIsReadable();
    QVERIFY2(ret == 0, QString("File is not readable `%1`")
        .arg(programArguments.file.fileName()).toStdString().c_str());

}

void uploaderTest::uploadFailTest() {
    if (disableAppInitialisation || disableAppNetworking)
        QSKIP("AppInitialisation||AppNetworking disabled");

    // generate body
    QString payload = UploaderLib::preparePayload(config.post_data, programArguments.parameter);
    //qInfo() << "payload: " + payload;
    QSignalSpy spy_error(&m_thread, &UploadThread::uploadError);

    // start worker thread
    const QString invalidFile("abc");
    m_thread.upload(invalidFile, 60, payload, config.header);

    QCOMPARE(spy_error.count(), 1);
    QList<QVariant> arguments = spy_error.takeFirst();

    QVERIFY2(UPLOAD_FILE_NOT_READABLE == arguments.at(0).toInt(), "Error expected upload to fail due to invalid file name.");
}

void uploaderTest::uploadTest() {
    if (disableAppInitialisation || disableAppNetworking)
        QSKIP("AppInitialisation||AppNetworking disabled");

    // disable stdout to prevent spamming test output
    //config.http_to_stdout = false; -> change this in config

    int ret = UploaderLib::fileIsReadable();
    if (ret != 0)
        QFAIL("Aborting test, cannot test file upload without a valid file.");

    // generate body
    QString payload = UploaderLib::preparePayload(config.post_data, programArguments.parameter);

    // add signals
    QSignalSpy spy_finish(&m_thread, &UploadThread::finished);
    QSignalSpy spy_response(&m_thread, &UploadThread::httpResponse);
    QSignalSpy spy_statuscode(&m_thread, &UploadThread::httpStatusCode);

    // start worker thread
    qInfo() << "Posting to: " << config.url;
    m_thread.upload(programArguments.file.fileName(), config.timeout, payload, config.header);

    QVERIFY(spy_finish.wait(15000000)); // wait 15 seconds for the signal
    //QList<QVariant> v = spy_statuscode.takeFirst();
    //qDebug() << "spy_statuscode: " << v;
    for (int waitDelay = 15000000; waitDelay > 0 && spy_statuscode.count() == 0 && spy_finish.count() == 0; waitDelay -= 100) {
        QTest::qWait(100);
    }
    QCOMPARE(spy_finish.count(), 1);
    QCOMPARE(spy_statuscode.count(), 1);

    // check statusCode
    QList<QVariant> statusCodeVariant = spy_statuscode.takeFirst();
    int statusCode = statusCodeVariant.at(0).toInt();
    QVERIFY2(statusCode == 200, QString("HTTP Statuscode 200 expected, got `%1`").arg(statusCode).toStdString().c_str());

    // check response body
    QList<QVariant> arguments = spy_response.takeFirst();
    //qInfo() << arguments.at(0).type();
    if (arguments.at(0).type() == QVariant::String) {
        m_responseHeaderAndBody = arguments.at(0).toString();
        //qInfo() << m_responseHeaderAndBody;
    }

    QVERIFY2(m_responseHeaderAndBody != "", "Empty Response, something went wrong.");
}

void uploaderTest::upload404Test() {
    if (disableAppInitialisation || disableAppNetworking)
        QSKIP("AppInitialisation||AppNetworking disabled");

    QString oldUrl(config.url);
    config.url += "-whatever";

    // generate body
    QString payload = UploaderLib::preparePayload(config.post_data, programArguments.parameter);

    // add signals
    QSignalSpy spy_finish(&m_thread, &UploadThread::finished);
    QSignalSpy spy_statuscode(&m_thread, &UploadThread::httpStatusCode);

    // start worker thread
    m_thread.upload(programArguments.file.fileName(), config.timeout, payload, config.header);

    QVERIFY(spy_finish.wait(15000000)); // wait 15 seconds for the signal
    QCOMPARE(spy_statuscode.count(), 1);

    // check statusCode
    QList<QVariant> statusCodeVariant = spy_statuscode.takeFirst();
    int statusCode = statusCodeVariant.at(0).toInt();
    QVERIFY2(statusCode == 404, QString("HTTP Statuscode 404 expected, got `%1`").arg(statusCode).toStdString().c_str());

    // reset url
    config.url = oldUrl;
}

// connection timeout
void uploaderTest::uploadTimeoutTest() {
    if (disableAppInitialisation || disableAppNetworking)
        QSKIP("AppInitialisation||AppNetworking disabled");

    QString oldUrl(config.url);
    config.url = "http://localhost:341536/";

    // generate body
    QString payload = UploaderLib::preparePayload(config.post_data, programArguments.parameter);
    qInfo() << "payload: " << payload;
    qInfo() << "programArguments.file: " << programArguments.file.fileName();

    // add signals
    QSignalSpy spy_finish(&m_thread, &UploadThread::finished);
    QSignalSpy spy_response(&m_thread, &UploadThread::uploadError);
    QSignalSpy spy_statuscode(&m_thread, &UploadThread::httpStatusCode);

    // start worker thread, with timeout 1 sec.
    qDebug() << "Starting thread";
    m_thread.upload(programArguments.file.fileName(), 1, payload, config.header);

    QVERIFY(spy_finish.wait(15000000)); // wait 15 seconds for the signal

    // check responseStatus (curl exit code)
    QList<QVariant> uploadErrorVariant = spy_response.takeFirst();
    int uploadError = uploadErrorVariant.at(0).toInt();
    QVERIFY2(uploadError == uploadError::UPLOAD_CURL_ERROR, QString("upload error expected 5, got `%1`").arg(uploadError).toStdString().c_str());

    // check statusCode

    //QList<QVariant> statusCodeVariant = spy_statuscode.takeFirst();
    //int statusCode = statusCodeVariant.at(0).toInt();
    //QVERIFY2(statusCode == 0, QString("HTTP Statuscode 404 expected, got `%1`").arg(statusCode).toStdString().c_str());

    // reset url
    config.url = oldUrl;
}

void uploaderTest::parseHtttpResponseLibTest() {
    httpResponse_t res;

    // test json
    QString httpResponse = "HTTP/1.1 200 OK\r\n"
    "Content-Type: application/json\r\n"
    "\r\n"
    R"("
{
    "attributes" : {
        "key1": 1,
        "key2: "two"
    }
}
)";

    res = UploaderLib::parseHtttpResponse(httpResponse);
    /*
    qInfo() << "error: " << res.error;
    qInfo() << "errorMessage: " << res.errorMessage;
    qInfo() << "content-type: " << res.contentType;
    qInfo() << "content-length: " << res.contentLength;
    qInfo() << "num header: " << res.header.size();
    qInfo() << "body: " << res.body;
    */

    QVERIFY(res.error == 0);
    QVERIFY(res.statusCode == 200);
    QVERIFY(res.errorMessage == "");
    QVERIFY(res.contentType == "application/json");

    // test empty body
    httpResponse = "HTTP/1.1 100 CONTINUE\r\n\r\n"
        "HTTP/1.1 200 OK\r\n"
        "Content-Type: application/json\r\n"
        "\r\n";

    res = UploaderLib::parseHtttpResponse(httpResponse);
    QVERIFY(res.error == 0);
    QVERIFY(res.statusCode == 200);
    QVERIFY(res.errorMessage == "");
    QVERIFY(res.contentType == "application/json");
    QVERIFY(res.contentLength == 0);

    // test invalid status line
    httpResponse = "HTTP/1.1 asd 200 OK\r\n"
        "Content-Type: application/json\r\n"
        "\r\n";

    res = UploaderLib::parseHtttpResponse(httpResponse);
    //qInfo() << res.error;
    qDebug() << "res.error: " << res.error;
    QVERIFY(res.error == 3);
    QVERIFY(res.statusCode == 0);

    // text plain content-type
    // test invalid status line
    httpResponse = "HTTP/1.1 200 OK\r\n"
        "\r\n";

    res = UploaderLib::parseHtttpResponse(httpResponse);
    //qInfo() << res.error;
    QVERIFY(res.error == 0);
    QVERIFY(res.statusCode == 200);
    QVERIFY(res.contentType == "text/plain");
    QVERIFY(res.contentLength == 0);
}

void uploaderTest::parseHtttpResponseTest() {
    if (disableAppInitialisation || disableAppNetworking)
        QSKIP("AppInitialisation||AppNetworking disabled");

    httpResponse_t res = UploaderLib::parseHtttpResponse(m_responseHeaderAndBody);
    QVERIFY2(res.error == 0, QString("expected [0] got: [%1]").arg(res.error).toStdString().c_str());
    QVERIFY2(res.statusCode == 200, QString("expected [200] got: [%1]").arg(res.statusCode).toStdString().c_str());
    QVERIFY2(res.errorMessage == "", QString("expected [\"\"] got: [%1]").arg(res.errorMessage).toStdString().c_str());
    QVERIFY2(res.contentType == "application/json", QString("expected [application/json] got: [%1]").arg(res.contentType).toStdString().c_str());
    //qDebug() << "res.contentType: " << res.contentType;

    qDebug() << "m_responseHeaderAndBody: " << m_responseHeaderAndBody;

    m_responseBody = res.body;
    m_responseHeader = res.header;


}

void uploaderTest::extractFromJsonLibTest() {

    if (disableJson)
        QSKIP("Json disabled");

    // success
    QString jsonContent = QString("{\"happy\": true, \"pi\": 3.141}");
    QString pointer = QString("/pi");
    jsonResult_t val = UploaderLib::extractFromJson(jsonContent, pointer);
    QVERIFY(val.error == 0);
    QVERIFY(val.errorMessage == "");
    //qInfo() << "ret: " << val.value.toFloat();
    QVERIFY2(val.value.toFloat() == 3.141f, QString("expected [3.141] got: [%1]").arg(val.value).toStdString().c_str());

    // malformed json
    jsonContent = QString("{'happy\": true, \"pi\": 3.141}");
    pointer = QString("/pi");
    val = UploaderLib::extractFromJson(jsonContent, pointer);
    QVERIFY(val.value == "");
    QVERIFY(val.errorMessage != "");
    QVERIFY2(val.error == 101, QString("expected [101] got: [%1]").arg(val.error).toStdString().c_str());

    // malformed json path
    jsonContent = QString("{\"happy\": true, \"pi\": 3.141}");
    pointer = QString("pi");
    val = UploaderLib::extractFromJson(jsonContent, pointer);
    QVERIFY(val.value == "");
    QVERIFY(val.errorMessage != "");
    QVERIFY2(val.error == 107, QString("expected [107] got: [%1]").arg(val.error).toStdString().c_str());

    // no scalar
    jsonContent = QString("{\"happy\": true, \"pi\": 3.141, \"array\": [1,2,3]}");
    pointer = QString("/array");
    val = UploaderLib::extractFromJson(jsonContent, pointer);
    QVERIFY(val.value == "");
    QVERIFY(val.errorMessage != "");
    QVERIFY2(val.error == -1, QString("expected [-1] got: [%1]").arg(val.error).toStdString().c_str());

    // no match
    jsonContent = QString("{\"happy\": true, \"pi\": 3.141, \"array\": [1,2,3]}");
    pointer = QString("/array_aaa");
    val = UploaderLib::extractFromJson(jsonContent, pointer);
    QVERIFY(val.value == "");
    QVERIFY(val.errorMessage != "");
    QVERIFY2(val.error == -3, QString("expected [-3] got: [%1]").arg(val.error).toStdString().c_str());
}

void uploaderTest::findResultVarsLibTest() {
    if (disableJson)
        QSKIP("Json disabled");

    jsonResultVar_t res;

    // working case
    // jsonResultVar_t UploaderLib::findResultVars(QHash<QString, QString> result_var, QString httpBody)
    QString httpBody = R"(
        {"attributes": {"pid": 8765, "fid": 4321}}
    )";
    QHash<QString, QString> result_var;
    result_var.insert("pid", "/attributes/pid");
    result_var.insert("fid", "/attributes/fid");
    res = UploaderLib::findResultVars(result_var, httpBody);

    QVERIFY(res.error == 0);
    QVERIFY(res.errorMessage == "");
    QVERIFY(res.variables.find("pid").value() == "8765");
    QVERIFY(res.variables.find("fid").value() == "4321");

    // error: invalid path
    result_var.clear();
    result_var.insert("pid", "/abc/pid");
    result_var.insert("fid", "/ributes/fid");
    res = UploaderLib::findResultVars(result_var, httpBody);

    QVERIFY2(res.error == -3, QString("res.error, expected [-3] got [%1]").arg(res.error).toStdString().c_str());
    QVERIFY(res.errorMessage != "");
    QVERIFY(res.variables.size() == 0);

    // error: invalid jsonpointer expression
    result_var.clear();
    result_var.insert("pid", "/abc~pid");
    res = UploaderLib::findResultVars(result_var, httpBody);

    //qInfo() << res.errorMessage;
    QVERIFY2(res.error == 108, QString("res.error, expected [108] got [%1]").arg(res.error).toStdString().c_str());
    QVERIFY(res.errorMessage != "");
    QVERIFY(res.variables.size() == 0);

}

void uploaderTest::extractFromJsonTest() {

    if (disableAppInitialisation || disableAppNetworking)
        QSKIP("AppInitialisation||AppNetworking disabled");

    // success
    QString jsonContent = QString("{\"happy\": true, \"pi\": 3.141}");
    QString pointer = QString("/pi");
    jsonResult_t val = UploaderLib::extractFromJson(jsonContent, pointer);
    QVERIFY(val.error == 0);
    QVERIFY(val.errorMessage == "");
    QVERIFY2(val.value.toFloat() == 3.141f, QString("expected [3.141] got: [%1]").arg(val.value).toStdString().c_str());

}

void uploaderTest::findResultVarsTest() {
    if (disableAppInitialisation || disableAppNetworking)
        QSKIP("AppInitialisation||AppNetworking disabled");

    jsonResultVar_t res = UploaderLib::findResultVars(config.result_var, m_responseBody);
    qDebug() << m_responseBody;
    qDebug() << config.result_var;
    qDebug() << res.errorMessage;
    QVERIFY(res.error == 0);
    QVERIFY(res.errorMessage == "");
    QVERIFY(res.variables.find("host").value() == "2");

    // remeber for later
    m_resultVars = res.variables;

}

void uploaderTest::substituteVariablesLibTest() {
    if (disableJson)
        QSKIP("Json disabled");

    varSubtitution_t res;

    // varSubtitution_t UploaderLib::substituteVariables(QHash<QString, QString> variables, QString text) {
    QString tpl("i am text and here we have a $pid and an $fid");
    QHash<QString, QString> vars;
    vars.insert("pid", "5678");
    vars.insert("fid", "1234");

    res = UploaderLib::substituteVariables(vars, tpl);
    QVERIFY(res.error == 0);
    QVERIFY(res.errorMessage == "");
    QVERIFY(res.value == "i am text and here we have a 5678 and an 1234");

    // error cases
    vars.clear();
    vars.insert("pid", "5678");

    res = UploaderLib::substituteVariables(vars, tpl);
    QVERIFY(res.error == 1);
    QVERIFY(res.errorMessage.right(4) == "$fid");
    QVERIFY(res.value == "");
}

void uploaderTest::substituteVariablesTest() {
    if (disableAppInitialisation || disableAppNetworking)
        QSKIP("AppInitialisation||AppNetworking disabled");

    varSubtitution_t res;

    res = UploaderLib::substituteVariables(m_resultVars, config.command);
    QVERIFY(res.error == 0);
    QVERIFY(res.errorMessage == "");
    QVERIFY(res.value == "http://google.com?q=host:+2");

}

void uploaderTest::getConfigVarLibTest() {
    // FIXME: add ini parser tests
    /*
    // test string
    QString v = UploaderLib::getConfigVar("url-", "with-param", "http://www.google.com?q=WTF").toString().trimmed();
    QVERIFY(v == "http://www.google.com?q=WTF");

    v = UploaderLib::getConfigVar("url", "with-param", "http://www.google.com?q=WTF").toString().trimmed();
    QVERIFY(v == "http://192.168.0.176/upload.php");


    // test int
    int t = UploaderLib::getConfigVar("timeout", "with-param", -1).toInt();
    QVERIFY2(t == 3, QString("Expected [3] got [%1]").arg(t).toStdString().c_str());

    t = UploaderLib::getConfigVar("timeout-", "with-param", -1).toInt();
    QVERIFY2(t == -1, QString("Expected [-1] got [%1]").arg(t).toStdString().c_str());
    */
}

void uploaderTest::preExecuteLibTest() {
    UploaderLib::UploaderLibError ret = UploaderLib::OK;

    // test success
    ret = UploaderLib::preExecute("cpp --version");
    QVERIFY(ret == 0);

    ret = UploaderLib::preExecute("");
    QVERIFY2(ret == UploaderLib::EXECUTE_CMD_EMPTY,
             QString("Expected [%1], got [%2]").arg(UploaderLib::EXECUTE_CMD_EMPTY).arg(ret).toStdString().c_str());

    ret = UploaderLib::preExecute("\"aaa");
    QVERIFY2(ret == UploaderLib::EXECUTE_QUTE_ERROR,
             QString("Expected [%1], got [%2]").arg(UploaderLib::EXECUTE_QUTE_ERROR).arg(ret).toStdString().c_str());

    ret = UploaderLib::preExecute("aaaaaa");
    QVERIFY2(ret == UploaderLib::EXECUTE_PROCESS_FAILED_TO_START,
             QString("Expected [%1], got [%2]").arg(UploaderLib::EXECUTE_PROCESS_FAILED_TO_START).arg(ret).toStdString().c_str());

    //qInfo() << "config.pre_command: " << config.pre_command;
    //ret = UploaderLib::preExecute(config.pre_command);
}
