#ifndef UPLOADERTEST_H
#define UPLOADERTEST_H

#include <QtTest/QtTest>
#include <QString>
#include <QFile>
#include <QProcessEnvironment>

#include "uploaderlib.h"
#include "uploadthread.h"
#include "globals.h"

extern bool disableAppInitialisation;
extern bool disableAppNetworking;
extern bool disableResponseParser;
extern bool disableJson;
extern bool disableIniParser;

class uploaderTest : public QObject
{
    Q_OBJECT
public:
    explicit uploaderTest(QObject *parent = nullptr);

private slots:

    void expandEnvironmentVariablesLibTest();

    // uploader tests
    void uploaderApplicationInitTest();

    // uploader setup
    void parseUrlLibTest();
    void parseCommandLineArgumentsTest();
    void readSettingsTest();
    void preparePayloadTest();
    void fileIsReadableTest();

    // settings
    void iniLibTest();
    void getConfigVarLibTest();

    void uploadFailTest();
    void uploadTest();
    void upload404Test();
    void uploadTimeoutTest();

    // http response parser
    void parseHtttpResponseTest();
    void parseHtttpResponseLibTest();

    // json tests
    void extractFromJsonLibTest();
    void extractFromJsonTest();
    void findResultVarsLibTest();
    void findResultVarsTest();
    void substituteVariablesLibTest();
    void substituteVariablesTest();

    // execute programs
    void preExecuteLibTest();

    // predefined
    void initTestCase();
    void cleanupTestCase();

    /*
    // predefines slots ////////////////////
    // called before the first test function
    void initTestCase();
    // called before every test function
    void init();
    // called after every test function
    void cleanup();
    // called after the last test function
    void cleanupTestCase();
    */

private slots:
    void httpResponse(QString responseHeaderAndBody);

private:
    UploadThread m_thread;
    QString m_responseHeaderAndBody;
    QString m_responseBody;
    QHash<QString, QString> m_responseHeader;
    QHash<QString, QString> m_resultVars;

};

#endif // UPLOADERTEST_H
