// http://imaginativethinking.ca/qtest-101-writing-unittests-qt-application/

#include <QCoreApplication>
#include "uploadertest.h"

// enable parts of the tests here to speed up testing
bool disableAppInitialisation = false;
bool disableAppNetworking = false;
bool disableResponseParser = false;
bool disableJson = false;
bool disableIniParser = false;

int main(int argc, char *argv[])
{
    // set application command line parameters
    const char *myargv[5];
    myargv[0] = "-c";
    myargv[1] = "-m=with-param";
    myargv[2] = "--fid=1234";
    myargv[3] = "--pid=5678";
    myargv[4] = "uploader.ini";
    int myargc = 5;

    // initialize application with our args
    QCoreApplication app(myargc, (char **) myargv);
    uploaderTest one;

    // initialize test with command line args from main()
    QTest::qExec(&one, argc, argv);
}
