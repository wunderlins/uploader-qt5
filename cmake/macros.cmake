################################################################################
# parse version numbers for rc files
# we expect [n].[n].[n][-TAG], if none is set, version defaults to 0.0.0
function (parse_version_string in-var out-var)
    set(MAJOR 0)
    set(MINOR 0)
    set(PATCH 0)
    set(TAG "")

    string(REPLACE "." ";" vlist ${in-var})
    list(LENGTH vlist list_len)
    if (${list_len} GREATER_EQUAL 1)
        list(GET vlist 0 MAJOR)
    endif()
    if (${list_len} GREATER_EQUAL 2)
        list(GET vlist 1 MINOR)
    endif()
    if (${list_len} GREATER_EQUAL 3)
        # check if we have a tag n-TAG
        list(GET vlist 2 minor)
        string(REPLACE "-" ";" tag ${minor})
        list(LENGTH tag list_len_tag)
        if (${list_len_tag} GREATER 1)
            list(GET tag 0 PATCH)
            list(GET tag 1 TAG)
        else() 
            list(GET vlist 2 PATCH)
        endif()
    endif()
    #message("MAJOR: ${MAJOR}, MINOR: ${MINOR}, PATCH: ${PATCH}, TAG: ${TAG}")
    set(${out-var} ${MAJOR} ${MINOR} ${PATCH} ${TAG} PARENT_SCOPE)
endfunction()

################################################################################
# create usage templates
#set(NAME "search")
function(usage_header NAME)
    string(TOUPPER ${NAME} NAMEU)
    string(TOLOWER ${NAME} NAMEL)
    set(VARNAME "${NAMEL}_usage")
    # read file
    file(READ ${CMAKE_CURRENT_LIST_DIR}/../resources/7${NAMEL}_usage.txt USAGE_TXT)
    # escape all backslashes
    string(REPLACE "\\" "\\\\" USAGE_TXT ${USAGE_TXT})
    # escape all quotes
    string(REPLACE "\"" "\\\"" USAGE_TXT ${USAGE_TXT})
    # replace newlines with string quotes
    string(REPLACE "\n" "\\n\"\n\"" USAGE_TXT ${USAGE_TXT})
    # add variable name to string
    string(CONCAT USAGE_TXT "const char *${VARNAME} = \"" ${USAGE_TXT} "\"")
    # add guards
    string(CONCAT USAGE_TXT "#ifndef _${NAMEU}_VERSION_H\n#define _${NAMEU}_VERSION_H\n\n" ${USAGE_TXT} "\;\n#endif // _${NAMEU}_VERSION_H\n")
    # write to destination file
    file(WRITE ${CMAKE_CURRENT_LIST_DIR}/../generated/usage_${NAMEL}.h ${USAGE_TXT})
    #message(${USAGE_TXT})
endfunction()

################################################################################
# create windows rc file
function(generate_rc
    in-file
    out-file
    version_list

    icon_file
    company_name
    product_description
    internal_name
    legal_copyright
)
    list(GET version_list 0 MAJOR)
    list(GET version_list 1 MINOR)
    list(GET version_list 2 PATCH)
    configure_file(${in-file} ${out-file} @ONLY)

    get_filename_component(icon_dir_in ${in-file} DIRECTORY)
    get_filename_component(icon_dir_out ${out-file} DIRECTORY)
    configure_file("${icon_dir_in}/${icon_file}" "${icon_dir_out}/${icon_file}" COPYONLY)

endfunction()