# do this after all install(...) commands so that all targets are finalized. 
# Essentially, the last thing included at the end of the top-level CMakeLists.txt

set(_fmt TGZ)
if(WIN32)
  set(_fmt ZIP)
endif()

set(CPACK_PACKAGE_NAME "${NAME}")
set(CPACK_PACKAGE_VENDOR "${PACKAGE_VENDOR}")
set(CPACK_PACKAGE_CONTACT "${PACKAGE_AUTHOR}")

set(CPACK_PACKAGE_VERSION "${VERSION_UPLOADER}")
list(GET VERSION_LIST 0 MAJOR)
list(GET VERSION_LIST 1 MINOR)
list(GET VERSION_LIST 2 PATCH)
set(CPACK_PACKAGE_VERSION_MAJOR "${MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${PATCH}")

set(CPACK_PACKAGE_DESCRIPTION ${NAME})
set(CPACK_PACKAGE_CONTACT "${PACKAGE_AUTHOR} ${APP_EMAIL}")

set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/resources/README.html")
set(CPACK_OUTPUT_FILE_PREFIX "${CMAKE_CURRENT_SOURCE_DIR}/release")
set(CPACK_PACKAGE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

# should we build a debian package?
# https://cmake.org/cmake/help/v3.3/module/CPackDeb.html
find_program(APT_AVAILABLE "apt")
if(APT_AVAILABLE AND NOT APPLE)
  list(APPEND _fmt "DEB")
  set(CPACK_DEBIAN_PACKAGE_DEPENDS, "libqt5gui5,libqt5widgets5")
  set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
  set(CPACK_DEBIAN_PACKAGE_DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION}")
endif()

# windows NSIS installer
if(WIN32)
  list(APPEND CPACK_GENERATOR "NSIS")
  set(CPACK_PACKAGE_INSTALL_DIRECTORY "${NAME}")
  set(CPACK_NSIS_INSTALLED_ICON_NAME "${CMAKE_CURRENT_SOURCE_DIR}/resources/icon.ico")
  set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_SOURCE_DIR}/resources/icon.ico")
  set(CPACK_NSIS_HELP_LINK ${APP_URL})
  set(CPACK_NSIS_URL_INFO_ABOUT ${APP_URL})
  set(CPACK_NSIS_CONTACT ${APP_EMAIL})
  set(CPACK_NSIS_DISPLAY_NAME ${NAME})
  set(CPACK_NSIS_PACKAGE_NAME ${NAME})

  #"${Qt5Core_DIR}/../../../bin/windeployqt.exe" --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "${CMAKE_CURRENT_BINARY_DIR}/viewer/7view.exe"
  #"${Qt5Core_DIR}/../../../bin/windeployqt64releaseonly.exe" --release --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "${CMAKE_CURRENT_BINARY_DIR}/uploader/uploader.exe"
  #"${Qt5Core_DIR}/../../../bin/windeployqt.exe" --release --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "${CMAKE_CURRENT_BINARY_DIR}/uploader/uploader.exe"
  # using a patched windeployqt.exe to work around a known bug in Qt 5.12-15
  # https://forum.qt.io/topic/109779/windeployqt-exe-comes-with-qt-5-14-not-copy-the-dlls-to-the-app-directory/7
  add_custom_target(Qt5deps ALL
    "${CMAKE_CURRENT_SOURCE_DIR}/windeployqt64releaseonly.exe" --release --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "${CMAKE_CURRENT_BINARY_DIR}/uploader/uploader.exe"
    DEPENDS uploader
    COMMENT "Finding Qt5 shared libraries"
    WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
  )

  #[[
  add_custom_command(OUTPUT Qt5deps uploader POST_BUILD
    COMMAND "${Qt5Core_DIR}/../../../bin/windeployqt64releaseonly.exe" --release --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "${CMAKE_CURRENT_BINARY_DIR}/uploader/uploader.exe"
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Finding Qt5 shared libraries"
  )
  ]]

  #[[
  install(CODE [[
    file(GET_RUNTIME_DEPENDENCIES
        RESOLVED_DEPENDENCIES_VAR RES
        UNRESOLVED_DEPENDENCIES_VAR UNRES
        CONFLICTING_DEPENDENCIES_PREFIX CONFLICTING_DEPENDENCIES
        LIBRARIES  ${MINGW64_BASE}/bin/libcurl-4.dll
        PRE_EXCLUDE_REGEXES /.*windows.*/

        message("\n\nFound dependencies :")
        foreach(DEP ${RES})
            message("${DEP}")
        endforeach()
        message("\n\nNot found dependencies :")
        foreach(DEP ${UNRES})
            message("${DEP}")
        endforeach()
    )
    ] ]
    LIBRARY DESTINATION "./")
    ]]

  # libcurl dependencies
  set(CURL_DEPS 
    ${MINGW64_BASE}/bin/libbrotlicommon.dll
    ${MINGW64_BASE}/bin/libbrotlidec.dll
    ${MINGW64_BASE}/bin/libcrypto-1_1-x64.dll
    ${MINGW64_BASE}/bin/libiconv-2.dll
    ${MINGW64_BASE}/bin/libidn2-0.dll
    ${MINGW64_BASE}/bin/libintl-8.dll
    ${MINGW64_BASE}/bin/libnghttp2-14.dll
    ${MINGW64_BASE}/bin/libpsl-5.dll
    ${MINGW64_BASE}/bin/libssh2-1.dll
    ${MINGW64_BASE}/bin/libssl-1_1-x64.dll
    ${MINGW64_BASE}/bin/libunistring-2.dll
    ${MINGW64_BASE}/bin/libwinpthread-1.dll
    ${MINGW64_BASE}/bin/libzstd.dll
    ${MINGW64_BASE}/bin/zlib1.dll
  )
  #install(FILES ${CURL_DEPS} DESTINATION "./")

  file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/qt_deps/")
  file(GET_RUNTIME_DEPENDENCIES
    EXECUTABLES "${MINGW64_BASE}/bin/libcurl-4.dll"
    RESOLVED_DEPENDENCIES_VAR found_deps
    UNRESOLVED_DEPENDENCIES_VAR unfound_deps
  )

  if (unfound_deps MATCHES ".*WINDOWS.*" OR unfound_deps MATCHES ".*windows.*")
    # not all required libraries found, probably error
    #message(ERROR "Not all dependencies of libcurl-4.dll found.")
    #continue()
  endif ()

  foreach (found_dep IN LISTS found_deps)
    message(STATUS "libcurl-4.dll dep: ${found_dep}")
    # Determine if the dep library should be copied.
    #if (dep_not_wanted)
    if (found_dep MATCHES ".*WINDOWS.*" OR found_dep MATCHES ".*windows.*")
      continue()
    endif ()
    configure_file("${found_dep}" "${CMAKE_CURRENT_BINARY_DIR}/qt_deps/" COPYONLY)
  endforeach ()

  # Copy all Qt5 assets to windows bin directory
  install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/qt_deps/" DESTINATION "./")

endif()

# OSX app bundle
# FIXME: may be bette to use qtmacdeploy to build Bundle, cpack Bundle 
#        seems not to be well documented
if(APPLE)
  list(APPEND CPACK_GENERATOR "BUNDLE")
  set(CPACK_BUNDLE_NAME ${NAME}) #: The bundle name
  set(CPACK_BUNDLE_ICON "${CMAKE_CURRENT_SOURCE_DIR}/resources/logo.icns") #: The bundle icon
  set(CPACK_BUNDLE_PLIST "${CMAKE_CURRENT_SOURCE_DIR}/generated/info.plist") #: The bundle plist

  # generated info plist
  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/resources/info.plist.in" "${CMAKE_CURRENT_SOURCE_DIR}/generated/info.plist")
endif()

# not .gitignore as its regex syntax is distinct
file(READ ${CMAKE_CURRENT_LIST_DIR}/cpack_ignore.txt _cpack_ignore)
string(REGEX REPLACE "\n" ";" _cpack_ignore ${_cpack_ignore})
set(CPACK_SOURCE_IGNORE_FILES "${_cpack_ignore}")
message("${CPACK_SOURCE_IGNORE_FILES}")

set(CPACK_GENERATOR ${_fmt})
set(CPACK_SOURCE_GENERATOR ${_fmt})
string(TOLOWER ${CMAKE_SYSTEM_NAME} _sys)
string(TOLOWER ${PROJECT_NAME} _project_lower)
set(CPACK_PACKAGE_FILE_NAME "${NAME}-${_sys}-${VERSION_UPLOADER}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${NAME}-${_sys}-${VERSION_UPLOADER}-source")

install(FILES ${CPACK_RESOURCE_FILE_README} ${CPACK_RESOURCE_FILE_LICENSE}
  DESTINATION share/docs/${PROJECT_NAME})

include(CPack)
